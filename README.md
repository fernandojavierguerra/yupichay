# yupichay

Gestion de Trazabilidad

## Libreria necesaria
LELParser

### Descargar proyecto y compilar

```
git clone git@gitlab.com:fernandojavierguerra/lel_agile.git
cd lel_agile/LELParser
mvn package
```

### Instalar .jar en repositorio local

desde el directorio del proyecto LELParser, utilizando el siguiente comando:
```
mvn install:install-file -Dfile=target/LELParser-0.0.1-SNAPSHOT.jar -DpomFile=pom.xml
```

## Ejecutar yupichay

```
git clone git@gitlab.com:fernandojavierguerra/yupichay.git

cd yupichay

mvn package

java -jar target/yupichay-0.0.1-SNAPSHOT.jar
```


Acceso aplicacion web yupichay: http://localhost:8080/

Acceso yupichay DB: http://localhost:8080/console

Consulta SQL

SELECT NOTION, SYMBOL_TYPE, QUALIFIED_NAME, TEXT
FROM SYMBOL_SNIPPET SYSN, SYMBOL SY, SNIPPET SN, SYMBOL_ITEM SYIT, ITEM IT
WHERE SYSN.SYMBOL_ID = SY.ID AND SN.ID = SYSN.SNIPPET_ID 
AND SYIT.SYMBOL_ID = SY.ID AND IT.ID = SYIT.ITEM_ID
AND SY.ID = 2

## Carga Datos

1. Acceder a BD yupichay a la URL http://localhost:8080/console

2. Cambiar JDBC URL por jdbc:h2:mem:testdb

3. From H2 console Execute: `RUNSCRIPT FROM 'bd/datos.sql';`

## Backup Datos

1. Access yupichay DB here: http://localhost:8080/console

2. From H2 console Execute: `SCRIPT TO 'bd/datos.sql';`
