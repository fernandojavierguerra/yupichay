/**
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


/**
 * Simple JavaBean domain object representing an owner.
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Michael Isvy
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "owners")
@unlp.fjguerra.yupichay.annotations.Trace(notion = "owner", symboltype = "object", item = "US9")
public class Owner extends org.springframework.samples.petclinic.model.Person {
    @javax.persistence.Column(name = "address")
    @javax.validation.constraints.NotEmpty
    private java.lang.String address;

    @javax.persistence.Column(name = "city")
    @javax.validation.constraints.NotEmpty
    private java.lang.String city;

    @javax.persistence.Column(name = "telephone")
    @javax.validation.constraints.NotEmpty
    @javax.validation.constraints.Digits(fraction = 0, integer = 10)
    private java.lang.String telephone;

    @javax.persistence.OneToMany(cascade = javax.persistence.CascadeType.ALL, mappedBy = "owner")
    private java.util.Set<org.springframework.samples.petclinic.owner.Pet> pets;

    public java.lang.String getAddress() {
        return this.address;
    }

    public void setAddress(java.lang.String address) {
        this.address = address;
    }

    public java.lang.String getCity() {
        return this.city;
    }

    public void setCity(java.lang.String city) {
        this.city = city;
    }

    public java.lang.String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(java.lang.String telephone) {
        this.telephone = telephone;
    }

    protected java.util.Set<org.springframework.samples.petclinic.owner.Pet> getPetsInternal() {
        if ((this.pets) == null) {
            this.pets = new java.util.HashSet<>();
        }
        return this.pets;
    }

    protected void setPetsInternal(java.util.Set<org.springframework.samples.petclinic.owner.Pet> pets) {
        this.pets = pets;
    }

    public java.util.List<org.springframework.samples.petclinic.owner.Pet> getPets() {
        java.util.List<org.springframework.samples.petclinic.owner.Pet> sortedPets = new java.util.ArrayList<>(getPetsInternal());
        org.springframework.beans.support.PropertyComparator.sort(sortedPets, new org.springframework.beans.support.MutableSortDefinition("name", true, true));
        return java.util.Collections.unmodifiableList(sortedPets);
    }

    @unlp.fjguerra.yupichay.annotations.Trace(notion = "add", symboltype = "verb")
    @unlp.fjguerra.yupichay.annotations.Trace(notion = "pet", symboltype = "object")
    public void addPet(org.springframework.samples.petclinic.owner.Pet pet) {
        if (pet.isNew()) {
            getPetsInternal().add(pet);
        }
        pet.setOwner(this);
    }

    @unlp.fjguerra.yupichay.annotations.Trace(notion = "add", symboltype = "verb")
    public void add(org.springframework.samples.petclinic.owner.Pet pet) {
        if (pet.isNew()) {
            getPetsInternal().add(pet);
        }
        pet.setOwner(this);
    }

    /**
     * Return the Pet with the given name, or null if none found for this Owner.
     *
     * @param name
     * 		to test
     * @return true if pet name is already in use
     */
    public org.springframework.samples.petclinic.owner.Pet getPet(java.lang.String name) {
        return getPet(name, false);
    }

    /**
     * Return the Pet with the given name, or null if none found for this Owner.
     *
     * @param name
     * 		to test
     * @return true if pet name is already in use
     */
    public org.springframework.samples.petclinic.owner.Pet getPet(java.lang.String name, boolean ignoreNew) {
        name = name.toLowerCase();
        for (org.springframework.samples.petclinic.owner.Pet pet : getPetsInternal()) {
            if ((!ignoreNew) || (!(pet.isNew()))) {
                java.lang.String compName = pet.getName();
                compName = compName.toLowerCase();
                if (compName.equals(name)) {
                    return pet;
                }
            }
        }
        return null;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return new org.springframework.core.style.ToStringCreator(this).append("id", this.getId()).append("new", this.isNew()).append("lastName", this.getLastName()).append("firstName", this.getFirstName()).append("address", this.address).append("city", this.city).append("telephone", this.telephone).toString();
    }
}

