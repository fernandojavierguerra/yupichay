/**
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


/**
 *
 *
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 */
@org.springframework.stereotype.Controller
@org.springframework.web.bind.annotation.RequestMapping("/owners/{ownerId}")
class PetController {
    private static final java.lang.String VIEWS_PETS_CREATE_OR_UPDATE_FORM = "pets/createOrUpdatePetForm";

    private final org.springframework.samples.petclinic.owner.PetRepository pets;

    private final org.springframework.samples.petclinic.owner.OwnerRepository owners;

    public PetController(org.springframework.samples.petclinic.owner.PetRepository pets, org.springframework.samples.petclinic.owner.OwnerRepository owners) {
        this.pets = pets;
        this.owners = owners;
    }

    @org.springframework.web.bind.annotation.ModelAttribute("types")
    public java.util.Collection<org.springframework.samples.petclinic.owner.PetType> populatePetTypes() {
        return this.pets.findPetTypes();
    }

    @org.springframework.web.bind.annotation.ModelAttribute("owner")
    public org.springframework.samples.petclinic.owner.Owner findOwner(@org.springframework.web.bind.annotation.PathVariable("ownerId")
    int ownerId) {
        return this.owners.findById(ownerId);
    }

    @org.springframework.web.bind.annotation.InitBinder("owner")
    public void initOwnerBinder(org.springframework.web.bind.WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @org.springframework.web.bind.annotation.InitBinder("pet")
    public void initPetBinder(org.springframework.web.bind.WebDataBinder dataBinder) {
        dataBinder.setValidator(new org.springframework.samples.petclinic.owner.PetValidator());
    }

    @org.springframework.web.bind.annotation.GetMapping("/pets/new")
    public java.lang.String initCreationForm(org.springframework.samples.petclinic.owner.Owner owner, org.springframework.ui.ModelMap model) {
        org.springframework.samples.petclinic.owner.Pet pet = new org.springframework.samples.petclinic.owner.Pet();
        owner.addPet(pet);
        model.put("pet", pet);
        return org.springframework.samples.petclinic.owner.PetController.VIEWS_PETS_CREATE_OR_UPDATE_FORM;
    }

    @org.springframework.web.bind.annotation.PostMapping("/pets/new")
    public java.lang.String processCreationForm(org.springframework.samples.petclinic.owner.Owner owner, @javax.validation.Valid
    org.springframework.samples.petclinic.owner.Pet pet, org.springframework.validation.BindingResult result, org.springframework.ui.ModelMap model) {
        if (((org.springframework.util.StringUtils.hasLength(pet.getName())) && (pet.isNew())) && ((owner.getPet(pet.getName(), true)) != null)) {
            result.rejectValue("name", "duplicate", "already exists");
        }
        owner.addPet(pet);
        if (result.hasErrors()) {
            model.put("pet", pet);
            return org.springframework.samples.petclinic.owner.PetController.VIEWS_PETS_CREATE_OR_UPDATE_FORM;
        } else {
            this.pets.save(pet);
            return "redirect:/owners/{ownerId}";
        }
    }

    @org.springframework.web.bind.annotation.GetMapping("/pets/{petId}/edit")
    public java.lang.String initUpdateForm(@org.springframework.web.bind.annotation.PathVariable("petId")
    int petId, org.springframework.ui.ModelMap model) {
        org.springframework.samples.petclinic.owner.Pet pet = this.pets.findById(petId);
        model.put("pet", pet);
        return org.springframework.samples.petclinic.owner.PetController.VIEWS_PETS_CREATE_OR_UPDATE_FORM;
    }

    @org.springframework.web.bind.annotation.PostMapping("/pets/{petId}/edit")
    @unlp.fjguerra.yupichay.annotations.Trace(notion = "update", symboltype = "verb")
    public java.lang.String processUpdateForm(@javax.validation.Valid
    org.springframework.samples.petclinic.owner.Pet pet, org.springframework.validation.BindingResult result, org.springframework.samples.petclinic.owner.Owner owner, org.springframework.ui.ModelMap model) {
        if (result.hasErrors()) {
            pet.setOwner(owner);
            model.put("pet", pet);
            return org.springframework.samples.petclinic.owner.PetController.VIEWS_PETS_CREATE_OR_UPDATE_FORM;
        } else {
            owner.addPet(pet);
            this.pets.save(pet);
            return "redirect:/owners/{ownerId}";
        }
    }
}

