/**
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


/**
 * Instructs Spring MVC on how to parse and print elements of type 'PetType'. Starting from Spring 3.0, Formatters have
 * come as an improvement in comparison to legacy PropertyEditors. See the following links for more details: - The
 * Spring ref doc: http://static.springsource.org/spring/docs/current/spring-framework-reference/html/validation.html#format-Formatter-SPI
 * - A nice blog entry from Gordon Dickens: http://gordondickens.com/wordpress/2010/09/30/using-spring-3-0-custom-type-converter/
 * <p/>
 *
 * @author Mark Fisher
 * @author Juergen Hoeller
 * @author Michael Isvy
 */
@org.springframework.stereotype.Component
public class PetTypeFormatter implements org.springframework.format.Formatter<org.springframework.samples.petclinic.owner.PetType> {
    private final org.springframework.samples.petclinic.owner.PetRepository pets;

    @org.springframework.beans.factory.annotation.Autowired
    public PetTypeFormatter(org.springframework.samples.petclinic.owner.PetRepository pets) {
        this.pets = pets;
    }

    @java.lang.Override
    public java.lang.String print(org.springframework.samples.petclinic.owner.PetType petType, java.util.Locale locale) {
        return petType.getName();
    }

    @java.lang.Override
    public org.springframework.samples.petclinic.owner.PetType parse(java.lang.String text, java.util.Locale locale) throws java.text.ParseException {
        java.util.Collection<org.springframework.samples.petclinic.owner.PetType> findPetTypes = this.pets.findPetTypes();
        for (org.springframework.samples.petclinic.owner.PetType type : findPetTypes) {
            if (type.getName().equals(text)) {
                return type;
            }
        }
        throw new java.text.ParseException(("type not found: " + text), 0);
    }
}

