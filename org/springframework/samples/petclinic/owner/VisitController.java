/**
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


/**
 *
 *
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 * @author Michael Isvy
 * @author Dave Syer
 */
@org.springframework.stereotype.Controller
class VisitController {
    private final org.springframework.samples.petclinic.visit.VisitRepository visits;

    private final org.springframework.samples.petclinic.owner.PetRepository pets;

    public VisitController(org.springframework.samples.petclinic.visit.VisitRepository visits, org.springframework.samples.petclinic.owner.PetRepository pets) {
        this.visits = visits;
        this.pets = pets;
    }

    @org.springframework.web.bind.annotation.InitBinder
    public void setAllowedFields(org.springframework.web.bind.WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * Called before each and every @RequestMapping annotated method.
     * 2 goals:
     * - Make sure we always have fresh data
     * - Since we do not use the session scope, make sure that Pet object always has an id
     * (Even though id is not part of the form fields)
     *
     * @param petId
     * 		
     * @return Pet
     */
    @org.springframework.web.bind.annotation.ModelAttribute("visit")
    public org.springframework.samples.petclinic.visit.Visit loadPetWithVisit(@org.springframework.web.bind.annotation.PathVariable("petId")
    int petId, java.util.Map<java.lang.String, java.lang.Object> model) {
        org.springframework.samples.petclinic.owner.Pet pet = this.pets.findById(petId);
        model.put("pet", pet);
        org.springframework.samples.petclinic.visit.Visit visit = new org.springframework.samples.petclinic.visit.Visit();
        pet.addVisit(visit);
        return visit;
    }

    // Spring MVC calls method loadPetWithVisit(...) before initNewVisitForm is called
    @org.springframework.web.bind.annotation.GetMapping("/owners/*/pets/{petId}/visits/new")
    public java.lang.String initNewVisitForm(@org.springframework.web.bind.annotation.PathVariable("petId")
    int petId, java.util.Map<java.lang.String, java.lang.Object> model) {
        return "pets/createOrUpdateVisitForm";
    }

    // Spring MVC calls method loadPetWithVisit(...) before processNewVisitForm is called
    @org.springframework.web.bind.annotation.PostMapping("/owners/{ownerId}/pets/{petId}/visits/new")
    public java.lang.String processNewVisitForm(@javax.validation.Valid
    org.springframework.samples.petclinic.visit.Visit visit, org.springframework.validation.BindingResult result) {
        if (result.hasErrors()) {
            return "pets/createOrUpdateVisitForm";
        } else {
            this.visits.save(visit);
            return "redirect:/owners/{ownerId}";
        }
    }
}

