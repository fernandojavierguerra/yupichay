package org.springframework.samples.petclinic.system;


/**
 * Cache configuration intended for caches providing the JCache API. This configuration creates the used cache for the
 * application and enables statistics that become accessible via JMX.
 */
@org.springframework.context.annotation.Configuration
@org.springframework.cache.annotation.EnableCaching
class CacheConfiguration {
    @org.springframework.context.annotation.Bean
    public org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer petclinicCacheConfigurationCustomizer() {
        return ( cm) -> {
            cm.createCache("vets", cacheConfiguration());
        };
    }

    /**
     * Create a simple configuration that enable statistics via the JCache programmatic configuration API.
     * <p>
     * Within the configuration object that is provided by the JCache API standard, there is only a very limited set of
     * configuration options. The really relevant configuration options (like the size limit) must be set via a
     * configuration mechanism that is provided by the selected JCache implementation.
     */
    private javax.cache.configuration.Configuration<java.lang.Object, java.lang.Object> cacheConfiguration() {
        return new javax.cache.configuration.MutableConfiguration().setStatisticsEnabled(true);
    }
}

