/**
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;


/**
 * Simple JavaBean domain object representing a veterinarian.
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Arjen Poutsma
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "vets")
public class Vet extends org.springframework.samples.petclinic.model.Person {
    @javax.persistence.ManyToMany(fetch = javax.persistence.FetchType.EAGER)
    @javax.persistence.JoinTable(name = "vet_specialties", joinColumns = @javax.persistence.JoinColumn(name = "vet_id"), inverseJoinColumns = @javax.persistence.JoinColumn(name = "specialty_id"))
    private java.util.Set<org.springframework.samples.petclinic.vet.Specialty> specialties;

    protected java.util.Set<org.springframework.samples.petclinic.vet.Specialty> getSpecialtiesInternal() {
        if ((this.specialties) == null) {
            this.specialties = new java.util.HashSet<>();
        }
        return this.specialties;
    }

    protected void setSpecialtiesInternal(java.util.Set<org.springframework.samples.petclinic.vet.Specialty> specialties) {
        this.specialties = specialties;
    }

    @javax.xml.bind.annotation.XmlElement
    public java.util.List<org.springframework.samples.petclinic.vet.Specialty> getSpecialties() {
        java.util.List<org.springframework.samples.petclinic.vet.Specialty> sortedSpecs = new java.util.ArrayList<>(getSpecialtiesInternal());
        org.springframework.beans.support.PropertyComparator.sort(sortedSpecs, new org.springframework.beans.support.MutableSortDefinition("name", true, true));
        return java.util.Collections.unmodifiableList(sortedSpecs);
    }

    public int getNrOfSpecialties() {
        return getSpecialtiesInternal().size();
    }

    public void addSpecialty(org.springframework.samples.petclinic.vet.Specialty specialty) {
        getSpecialtiesInternal().add(specialty);
    }
}

