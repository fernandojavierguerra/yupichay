/**
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;


/**
 *
 *
 * @author Juergen Hoeller
 * @author Mark Fisher
 * @author Ken Krebs
 * @author Arjen Poutsma
 */
@org.springframework.stereotype.Controller
class VetController {
    private final org.springframework.samples.petclinic.vet.VetRepository vets;

    public VetController(org.springframework.samples.petclinic.vet.VetRepository clinicService) {
        this.vets = clinicService;
    }

    @org.springframework.web.bind.annotation.GetMapping("/vets.html")
    public java.lang.String showVetList(java.util.Map<java.lang.String, java.lang.Object> model) {
        // Here we are returning an object of type 'Vets' rather than a collection of Vet
        // objects so it is simpler for Object-Xml mapping
        org.springframework.samples.petclinic.vet.Vets vets = new org.springframework.samples.petclinic.vet.Vets();
        vets.getVetList().addAll(this.vets.findAll());
        model.put("vets", vets);
        return "vets/vetList";
    }

    @org.springframework.web.bind.annotation.GetMapping({ "/vets" })
    @org.springframework.web.bind.annotation.ResponseBody
    public org.springframework.samples.petclinic.vet.Vets showResourcesVetList() {
        // Here we are returning an object of type 'Vets' rather than a collection of Vet
        // objects so it is simpler for JSon/Object mapping
        org.springframework.samples.petclinic.vet.Vets vets = new org.springframework.samples.petclinic.vet.Vets();
        vets.getVetList().addAll(this.vets.findAll());
        return vets;
    }
}

