/**
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.model;


/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Ken Krebs
 */
@javax.persistence.MappedSuperclass
public class Person extends org.springframework.samples.petclinic.model.BaseEntity {
    @javax.persistence.Column(name = "first_name")
    @javax.validation.constraints.NotEmpty
    private java.lang.String firstName;

    @javax.persistence.Column(name = "last_name")
    @javax.validation.constraints.NotEmpty
    private java.lang.String lastName;

    public java.lang.String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }

    public java.lang.String getLastName() {
        return this.lastName;
    }

    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }
}

