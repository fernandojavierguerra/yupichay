/**
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


/**
 *
 *
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 * @author Michael Isvy
 */
@org.springframework.stereotype.Controller
class OwnerController {
    private static final java.lang.String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "owners/createOrUpdateOwnerForm";

    private final org.springframework.samples.petclinic.owner.OwnerRepository owners;

    public OwnerController(org.springframework.samples.petclinic.owner.OwnerRepository clinicService) {
        this.owners = clinicService;
    }

    @org.springframework.web.bind.annotation.InitBinder
    public void setAllowedFields(org.springframework.web.bind.WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @org.springframework.web.bind.annotation.GetMapping("/owners/new")
    public java.lang.String initCreationForm(java.util.Map<java.lang.String, java.lang.Object> model) {
        org.springframework.samples.petclinic.owner.Owner owner = new org.springframework.samples.petclinic.owner.Owner();
        model.put("owner", owner);
        return org.springframework.samples.petclinic.owner.OwnerController.VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
    }

    @org.springframework.web.bind.annotation.PostMapping("/owners/new")
    public java.lang.String processCreationForm(@javax.validation.Valid
    org.springframework.samples.petclinic.owner.Owner owner, org.springframework.validation.BindingResult result) {
        if (result.hasErrors()) {
            return org.springframework.samples.petclinic.owner.OwnerController.VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
        } else {
            this.owners.save(owner);
            return "redirect:/owners/" + (owner.getId());
        }
    }

    @org.springframework.web.bind.annotation.GetMapping("/owners/find")
    public java.lang.String initFindForm(java.util.Map<java.lang.String, java.lang.Object> model) {
        model.put("owner", new org.springframework.samples.petclinic.owner.Owner());
        return "owners/findOwners";
    }

    @org.springframework.web.bind.annotation.GetMapping("/owners")
    public java.lang.String processFindForm(org.springframework.samples.petclinic.owner.Owner owner, org.springframework.validation.BindingResult result, java.util.Map<java.lang.String, java.lang.Object> model) {
        // allow parameterless GET request for /owners to return all records
        if ((owner.getLastName()) == null) {
            owner.setLastName("");// empty string signifies broadest possible search

        }
        // find owners by last name
        java.util.Collection<org.springframework.samples.petclinic.owner.Owner> results = this.owners.findByLastName(owner.getLastName());
        if (results.isEmpty()) {
            // no owners found
            result.rejectValue("lastName", "notFound", "not found");
            return "owners/findOwners";
        } else
            if ((results.size()) == 1) {
                // 1 owner found
                owner = results.iterator().next();
                return "redirect:/owners/" + (owner.getId());
            } else {
                // multiple owners found
                model.put("selections", results);
                return "owners/ownersList";
            }

    }

    @org.springframework.web.bind.annotation.GetMapping("/owners/{ownerId}/edit")
    public java.lang.String initUpdateOwnerForm(@org.springframework.web.bind.annotation.PathVariable("ownerId")
    int ownerId, org.springframework.ui.Model model) {
        org.springframework.samples.petclinic.owner.Owner owner = this.owners.findById(ownerId);
        model.addAttribute(owner);
        return org.springframework.samples.petclinic.owner.OwnerController.VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
    }

    @org.springframework.web.bind.annotation.PostMapping("/owners/{ownerId}/edit")
    public java.lang.String processUpdateOwnerForm(@javax.validation.Valid
    org.springframework.samples.petclinic.owner.Owner owner, org.springframework.validation.BindingResult result, @org.springframework.web.bind.annotation.PathVariable("ownerId")
    int ownerId) {
        if (result.hasErrors()) {
            return org.springframework.samples.petclinic.owner.OwnerController.VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
        } else {
            owner.setId(ownerId);
            this.owners.save(owner);
            return "redirect:/owners/{ownerId}";
        }
    }

    /**
     * Custom handler for displaying an owner.
     *
     * @param ownerId
     * 		the ID of the owner to display
     * @return a ModelMap with the model attributes for the view
     */
    @org.springframework.web.bind.annotation.GetMapping("/owners/{ownerId}")
    public org.springframework.web.servlet.ModelAndView showOwner(@org.springframework.web.bind.annotation.PathVariable("ownerId")
    int ownerId) {
        org.springframework.web.servlet.ModelAndView mav = new org.springframework.web.servlet.ModelAndView("owners/ownerDetails");
        mav.addObject(this.owners.findById(ownerId));
        return mav;
    }
}

