/**
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


/**
 * Simple business object representing a pet.
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 * @author Sam Brannen
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "pets")
public class Pet extends org.springframework.samples.petclinic.model.NamedEntity {
    @javax.persistence.Column(name = "birth_date")
    @org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd")
    private java.time.LocalDate birthDate;

    @javax.persistence.ManyToOne
    @javax.persistence.JoinColumn(name = "type_id")
    private org.springframework.samples.petclinic.owner.PetType type;

    @javax.persistence.ManyToOne
    @javax.persistence.JoinColumn(name = "owner_id")
    private org.springframework.samples.petclinic.owner.Owner owner;

    @javax.persistence.OneToMany(cascade = javax.persistence.CascadeType.ALL, mappedBy = "petId", fetch = javax.persistence.FetchType.EAGER)
    private java.util.Set<org.springframework.samples.petclinic.visit.Visit> visits = new java.util.LinkedHashSet<>();

    public void setBirthDate(java.time.LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public java.time.LocalDate getBirthDate() {
        return this.birthDate;
    }

    public org.springframework.samples.petclinic.owner.PetType getType() {
        return this.type;
    }

    public void setType(org.springframework.samples.petclinic.owner.PetType type) {
        this.type = type;
    }

    public org.springframework.samples.petclinic.owner.Owner getOwner() {
        return this.owner;
    }

    protected void setOwner(org.springframework.samples.petclinic.owner.Owner owner) {
        this.owner = owner;
    }

    protected java.util.Set<org.springframework.samples.petclinic.visit.Visit> getVisitsInternal() {
        if ((this.visits) == null) {
            this.visits = new java.util.HashSet<>();
        }
        return this.visits;
    }

    protected void setVisitsInternal(java.util.Set<org.springframework.samples.petclinic.visit.Visit> visits) {
        this.visits = visits;
    }

    public java.util.List<org.springframework.samples.petclinic.visit.Visit> getVisits() {
        java.util.List<org.springframework.samples.petclinic.visit.Visit> sortedVisits = new java.util.ArrayList<>(getVisitsInternal());
        org.springframework.beans.support.PropertyComparator.sort(sortedVisits, new org.springframework.beans.support.MutableSortDefinition("date", false, false));
        return java.util.Collections.unmodifiableList(sortedVisits);
    }

    public void addVisit(org.springframework.samples.petclinic.visit.Visit visit) {
        getVisitsInternal().add(visit);
        visit.setPetId(this.getId());
    }
}

