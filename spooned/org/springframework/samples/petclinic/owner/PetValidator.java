/**
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


/**
 * <code>Validator</code> for <code>Pet</code> forms.
 * <p>
 * We're not using Bean Validation annotations here because it is easier to define such validation rule in Java.
 * </p>
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 */
public class PetValidator implements org.springframework.validation.Validator {
    private static final java.lang.String REQUIRED = "required";

    @java.lang.Override
    public void validate(java.lang.Object obj, org.springframework.validation.Errors errors) {
        org.springframework.samples.petclinic.owner.Pet pet = ((org.springframework.samples.petclinic.owner.Pet) (obj));
        java.lang.String name = pet.getName();
        // name validation
        if (!(org.springframework.util.StringUtils.hasLength(name))) {
            errors.rejectValue("name", org.springframework.samples.petclinic.owner.PetValidator.REQUIRED, org.springframework.samples.petclinic.owner.PetValidator.REQUIRED);
        }
        // type validation
        if ((pet.isNew()) && ((pet.getType()) == null)) {
            errors.rejectValue("type", org.springframework.samples.petclinic.owner.PetValidator.REQUIRED, org.springframework.samples.petclinic.owner.PetValidator.REQUIRED);
        }
        // birth date validation
        if ((pet.getBirthDate()) == null) {
            errors.rejectValue("birthDate", org.springframework.samples.petclinic.owner.PetValidator.REQUIRED, org.springframework.samples.petclinic.owner.PetValidator.REQUIRED);
        }
    }

    /**
     * This Validator validates *just* Pet instances
     */
    @java.lang.Override
    public boolean supports(java.lang.Class<?> clazz) {
        return org.springframework.samples.petclinic.owner.Pet.class.isAssignableFrom(clazz);
    }
}

