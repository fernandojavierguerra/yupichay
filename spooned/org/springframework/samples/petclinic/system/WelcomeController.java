package org.springframework.samples.petclinic.system;


@org.springframework.stereotype.Controller
class WelcomeController {
    @org.springframework.web.bind.annotation.GetMapping("/")
    public java.lang.String welcome() {
        return "welcome";
    }
}

