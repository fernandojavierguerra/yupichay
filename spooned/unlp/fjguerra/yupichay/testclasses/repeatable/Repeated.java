package unlp.fjguerra.yupichay.testclasses.repeatable;


@unlp.fjguerra.yupichay.testclasses.repeatable.Tag("machin")
@unlp.fjguerra.yupichay.testclasses.repeatable.Tag("truc")
@unlp.fjguerra.yupichay.annotations.Trace(notion = "estimator", symboltype = "subject")
@unlp.fjguerra.yupichay.annotations.Trace(notion = "game", symboltype = "object")
public class Repeated {
    public void method() {
    }

    public void withoutAnnotation() {
    }
}

