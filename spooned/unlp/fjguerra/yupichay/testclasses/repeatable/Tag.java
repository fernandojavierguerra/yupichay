package unlp.fjguerra.yupichay.testclasses.repeatable;


@java.lang.annotation.Repeatable(unlp.fjguerra.yupichay.testclasses.repeatable.Tags.class)
public @interface Tag {
    java.lang.String value() default "";
}

