package unlp.fjguerra.yupichay.testclasses.repeatable;


public @interface Tags {
    unlp.fjguerra.yupichay.testclasses.repeatable.Tag[] value();
}

