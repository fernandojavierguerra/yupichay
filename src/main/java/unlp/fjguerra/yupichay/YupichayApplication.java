package unlp.fjguerra.yupichay;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import lifia.lelparser.model.UserStory;
import lifia.lelparser.model.parserResults.ParserResult;
import unlp.fjguerra.yupichay.domain.CodeAnalisysStrategy;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.ProjectRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;

@SpringBootApplication
public class YupichayApplication {

	@Autowired
	private SymbolRepository symbolRepository;
	@Autowired
	private ItemRepository itemRepository;
	@Autowired
	private SnippetRepository snippetRepository;
	@Autowired
	private ProjectRepository proyectoRepository;

	public static void main(String[] args) {
		SpringApplication.run(YupichayApplication.class, args);
	};

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

			System.out.println("********************  ***************");

			Project proyecto = new Project();
			proyecto.setOutputPath("target/spooned/");
			
//			proyecto.setInputPath("./src/main/resources/project/petclinic/src/main/java/org/springframework/samples/petclinic");
//        	proyecto.setProgrammingLanguage("java");
//        	proyecto.setItemsPath("/project/project/petclinic");
        	
			proyecto.setInputPath("./src/main/resources/project/Sylius/src/Sylius");
			proyecto.setProgrammingLanguage("php");
			proyecto.setItemsPath("./src/main/resources/project/Sylius/features");
			
//        	proyecto.setItemsPath("./src/main/resources/project/Sylius/features/channel");
//			proyecto.setInputPath("./src/main/resources/project/Sylius/src/Sylius/Bundle/AdminBundle/Twig");
        	

			proyectoRepository.save(proyecto);
			
			
        	//Save items to BBDD
//        	Collection<UserStory> userStories = proyecto.buildSampleUserStories();
//			Collection<UserStory> userStories = proyecto.readItems();
			
//        	Collection<UserStory> userStories = proyecto.readUS();
//        	proyecto.saveItems(userStories, itemRepository);
//        	proyecto.createEpicItems(itemRepository);
//        	
        	//Save symbols to BBDD
//        	ParserResult result = proyecto.parseItems(userStories);
//        	proyecto.getSymbolsFromResult(result, symbolRepository);
//        	proyecto.linkItemsWithSymbols(result, itemRepository, symbolRepository);
        	
        	
//        	proyecto.annotateCodeWithObjects(symbolRepository, snippetRepository);
//        	proyecto.annotateCodeWithSubjects(symbolRepository, snippetRepository);
//        	proyecto.annotateCodeWithVerbs(symbolRepository, snippetRepository);
//        	proyecto.getSnippetsFromCode(symbolRepository, snippetRepository);
        	
        


		};
	};

}
