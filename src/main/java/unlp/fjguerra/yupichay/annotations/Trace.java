package unlp.fjguerra.yupichay.annotations;

import java.lang.annotation.Repeatable;

@Repeatable(Traces.class)
public @interface Trace {
	String notion();
	String symboltype();
	String item();
}
