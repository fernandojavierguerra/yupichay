package unlp.fjguerra.yupichay.annotations;

public @interface Traces {
	Trace[] value();
}
