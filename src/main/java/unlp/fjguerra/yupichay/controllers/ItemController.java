package unlp.fjguerra.yupichay.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import unlp.fjguerra.yupichay.repository.ItemRepository;

@Controller
public class ItemController {
	
	private ItemRepository itemRepository; 
	
	@Autowired
	public void setItemRepository(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}	
	
	@RequestMapping(value = "/items/getItems", method = RequestMethod.GET)
	public String getItems(@RequestParam (value = "search", required = false) String search, Model model){
		
		if (search == null || search.isEmpty()) {
			model.addAttribute("items", itemRepository.findAll());
		} else
		{
			model.addAttribute("items", itemRepository.findByTextContainingIgnoreCase(search));
		}
		System.out.println("Returning items:");
		return "items";
	}
	
	@RequestMapping(value = "/items/getEpicItems", method = RequestMethod.GET)
	public String getEpicItems(Model model){
		model.addAttribute("items", itemRepository.findEpicItems());
		System.out.println("Returning epic items:");
		return "items";
	}
	
	@RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
	public String showItem(@PathVariable String id, Model model){
		itemRepository.findById(id).ifPresent(o -> model.addAttribute("item", o));
		System.out.println("Returning item:"+itemRepository.findById(id).toString());
		return "item";
	}


}
