package unlp.fjguerra.yupichay.controllers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lifia.lelparser.exceptions.InvalidRequirementFormatException;
import lifia.lelparser.model.UserStory;
import lifia.lelparser.model.parserResults.ParserResult;
import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.domain.CodeFragment;
import unlp.fjguerra.yupichay.domain.CodeFragmentSelected;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.ProjectRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

@Controller
public class ProcessorController {
	
	private ProjectRepository projectRepository;
	private SnippetRepository snippetRepository;
	private SymbolRepository symbolRepository;
	private ItemRepository itemRepository;
	
	@Autowired
	public void setSnippetRepository(SnippetRepository snippetRepository) {
		this.snippetRepository = snippetRepository;
	}
	@Autowired
	public void setSymbolRepository(SymbolRepository symbolRepository) {
		this.symbolRepository = symbolRepository;
	}
	
	@Autowired
	public void setItemRepository(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	@Autowired
	public void setProjectRepository(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}
	
	@RequestMapping(value = "/processor/readItems", method = RequestMethod.GET)
	public String readItemsFromFile(Model model) throws InvalidRequirementFormatException, IOException, URISyntaxException{
		Project proyecto = projectRepository.findById((long) 1).orElse(null);        	
		//Save items to BBDD
    	Collection<UserStory> userStories = proyecto.readItems();
    	proyecto.saveItems(userStories, itemRepository);
    	
//    	//Save symbols to BBDD
    	ParserResult result = proyecto.parseItems(userStories);
    	proyecto.getSymbolsFromResult(result, symbolRepository);

		System.out.println("Reading items from file:");
		return "redirect:/items/getItems";
	}
	
	@RequestMapping(value = "/processor/findObjects", method = RequestMethod.GET)
	public String findObjects(Model model) {
		Project proyecto = projectRepository.findById((long) 1).orElse(null);
		
		List<CodeFragment> codeFragments = proyecto.annotateCodeWithObjects(symbolRepository, snippetRepository);
		CodeFragmentSelected codeFragmentsSelected = new CodeFragmentSelected();
		codeFragmentsSelected.setCodeFragments(codeFragments);
		model.addAttribute("codeFragmentsSelected", codeFragmentsSelected);
		return "snippetsobjectsform";
	}
	
	@RequestMapping(value = "/processor/findObjects", method = RequestMethod.POST)
	public String viewObjects(@ModelAttribute CodeFragmentSelected codeFragmentsSelected, Model model) {
		
		for (CodeFragment codeFragment: codeFragmentsSelected.getCodeFragments()) {
			if (codeFragment.getSelected()) {
				FileUtility fileRelpace = new FileUtility();
				fileRelpace.insertBefore(codeFragment.getTarget(), codeFragment.getInsertString(), codeFragment.getFileName(), codeFragment.getTypeTarget(), codeFragment.getLineNumber());		
			}
		}
		return "snippetsobjectsform";
	}
	
	@RequestMapping(value = "/processor/findSubjects", method = RequestMethod.GET)
	public String findSubjects(Model model) {
		Project proyecto = projectRepository.findById((long) 1).orElse(null);
		
		List<CodeFragment> codeFragments = proyecto.annotateCodeWithSubjects(symbolRepository, snippetRepository);
		CodeFragmentSelected codeFragmentsSelected = new CodeFragmentSelected();
		codeFragmentsSelected.setCodeFragments(codeFragments);
		model.addAttribute("codeFragmentsSelected", codeFragmentsSelected);
		return "snippetssubjectsform";
	}
	
	@RequestMapping(value = "/processor/findSubjects", method = RequestMethod.POST)
	public String viewSubjects(@ModelAttribute CodeFragmentSelected codeFragmentsSelected, Model model) {
		
		for (CodeFragment codeFragment: codeFragmentsSelected.getCodeFragments()) {
			if (codeFragment.getSelected()) {
				FileUtility fileRelpace = new FileUtility();
				fileRelpace.insertBefore(codeFragment.getTarget(), codeFragment.getInsertString(), codeFragment.getFileName(), codeFragment.getTypeTarget(), codeFragment.getLineNumber());		
			}
		}
		return "snippetssubjectsform";
	}
	
	@RequestMapping(value = "/processor/findVerbs", method = RequestMethod.GET)
	public String findVerbs(Model model) {
		
		Project proyecto = projectRepository.findById((long) 1).orElse(null);
		List<CodeFragment> codeFragments = proyecto.annotateCodeWithVerbs(symbolRepository, snippetRepository);
		CodeFragmentSelected codeFragmentsSelected = new CodeFragmentSelected();
		codeFragmentsSelected.setCodeFragments(codeFragments);
		model.addAttribute("codeFragmentsSelected", codeFragmentsSelected);

		System.out.println("Finding Verbs:");
		return "snippetsmethodsform";
	}
	
	@RequestMapping(value = "/processor/findVerbs", method = RequestMethod.POST)
	public String viewVerbs(@ModelAttribute CodeFragmentSelected codeFragmentsSelected, Model model) {
		
		for (CodeFragment codeFragment: codeFragmentsSelected.getCodeFragments()) {
			if (codeFragment.getSelected()) {
				System.out.println("Code fragment: "+codeFragment.toString());
				FileUtility fileRelpace = new FileUtility();
				fileRelpace.insertBefore(codeFragment.getTarget(), codeFragment.getInsertString(), codeFragment.getFileName(), codeFragment.getTypeTarget(), codeFragment.getLineNumber());		
			}
		}
		return "snippetsmethodsform";
	}
	
	@RequestMapping(value = "/processor/findSnippets", method = RequestMethod.GET)
	public String findSnippets(Model model) {
		
		Project proyecto = projectRepository.findById((long) 1).orElse(null);
		proyecto.getSnippetsFromCode(symbolRepository, snippetRepository, itemRepository);
		System.out.println("Finding Snippets:");
		return "redirect:/snippets/getSnippets";
	}
	
	@RequestMapping(value = "/processor/linkSymbols", method = RequestMethod.GET)
	public String linkSymbols(Model model) throws InvalidRequirementFormatException, IOException, URISyntaxException{
		Project proyecto = projectRepository.findById((long) 1).orElse(null);
		//Collection<UserStory> userStories = proyecto.buildSampleUserStories();
		Collection<UserStory> userStories = proyecto.readItems();
		ParserResult result = proyecto.parseItems(userStories);
		proyecto.linkItemsWithSymbols(result, itemRepository, symbolRepository);
		model.addAttribute("symbols", symbolRepository.findAll());
		System.out.println("Linking symbols:");
		return "redirect:/items/getItems";
	}
	
	@RequestMapping(value = "/processor/listFindSymbol", method = RequestMethod.GET)
	public String listFindSymbol(Model model) throws InvalidRequirementFormatException, IOException, URISyntaxException{
		model.addAttribute("symbols", symbolRepository.findByOrderByNotion());
		System.out.println("Returning symbols:");
		return "listfindsymbol";
	}
	
	@RequestMapping(value = "/processor/findSymbol/{id}", method = RequestMethod.GET)
	public String findSymbol(@PathVariable Long id, Model model) throws InvalidRequirementFormatException, IOException, URISyntaxException{
		Project proyecto = projectRepository.findById((long) 1).orElse(null);
		List<CodeFragment> codeFragments = proyecto.annotateCodeWithSymbol(symbolRepository, snippetRepository, id);
		CodeFragmentSelected codeFragmentsSelected = new CodeFragmentSelected();
		codeFragmentsSelected.setCodeFragments(codeFragments);
		model.addAttribute("codeFragmentsSelected", codeFragmentsSelected);

		System.out.println("Finding Symbol:");
		return "snippetssymbolform";
	}
	
	@RequestMapping(value = "/processor/findSymbolFromItem/{symbolId}/{itemId}", method = RequestMethod.GET)
	public String findSymbolFromItem(@PathVariable Long symbolId, @PathVariable String itemId, Model model) throws InvalidRequirementFormatException, IOException, URISyntaxException{
		Project proyecto = projectRepository.findById((long) 1).orElse(null);
		List<CodeFragment> codeFragments = proyecto.annotateCodeWithSymbolFromItem(symbolRepository, snippetRepository, itemRepository, symbolId, itemId);
		CodeFragmentSelected codeFragmentsSelected = new CodeFragmentSelected();
		codeFragmentsSelected.setCodeFragments(codeFragments);
		model.addAttribute("codeFragmentsSelected", codeFragmentsSelected);

		System.out.println("Finding Symbol:");
		return "snippetssymbolform";
	}
	
	@RequestMapping(value = "/processor/findSymbol", method = RequestMethod.POST)
	public String findSymbol(@ModelAttribute CodeFragmentSelected codeFragmentsSelected, Model model) {
		
		for (CodeFragment codeFragment: codeFragmentsSelected.getCodeFragments()) {
			if (codeFragment.getSelected()) {
				System.out.println("Code fragment: "+codeFragment.toString());
				FileUtility fileRelpace = new FileUtility();
				fileRelpace.insertBefore(codeFragment.getTarget(), codeFragment.getInsertString(), codeFragment.getFileName(), codeFragment.getTypeTarget(), codeFragment.getLineNumber());		
			}
		}
		return "snippetssymbolform";
	}

}
