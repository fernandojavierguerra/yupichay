package unlp.fjguerra.yupichay.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.repository.ProjectRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;

@Controller
public class ProjectController {
	
	private ProjectRepository projectRepository;
	private SnippetRepository snippetRepository;
	private SymbolRepository symbolRepository;
	
	 
	@Autowired
	public void setSnippetRepository(SnippetRepository snippetRepository) {
		this.snippetRepository = snippetRepository;
	}
	@Autowired
	public void setSymbolRepository(SymbolRepository symbolRepository) {
		this.symbolRepository = symbolRepository;
	}

	@Autowired
	public void setProjectRepository(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}
	
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public String list(Model model){
		model.addAttribute("projects", projectRepository.findAll());
		System.out.println("Returning projects:");
		return "projects";
	}

	@RequestMapping(value = "/project/{id}", method = RequestMethod.GET)
	public String showProject(@PathVariable Long id, Model model) {
		projectRepository.findById(id).ifPresent(o -> model.addAttribute("project", o));
		System.out.println("Returning project:");
		return "project";
	}
	
	@RequestMapping(value = "/project/edit/{id}", method = RequestMethod.GET)
	public String editProject(@PathVariable Long id, Model model) {
		projectRepository.findById(id).ifPresent(o -> model.addAttribute("project", o));
		System.out.println("Returning project:");
		return "projectform";
	}
	
	@RequestMapping(value = "/project", method = RequestMethod.POST)
	public String saveProject(Project project) {
		projectRepository.save(project);
		System.out.println("Saving project:");
		return "redirect:/project";
	}


}
