package unlp.fjguerra.yupichay.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import unlp.fjguerra.yupichay.repository.SnippetRepository;

@Controller
public class SnippetController {
	private SnippetRepository snippetRepository;
	
	@Autowired
	public void setSnippetRepository(SnippetRepository snippetRepository) {
		this.snippetRepository = snippetRepository;
	}
	
	@RequestMapping(value = "/snippets/getSnippets", method = RequestMethod.GET)
	public String list(Model model){
		model.addAttribute("snippets", snippetRepository.findAll());
		System.out.println("Returning snippets:");
		return "snippets";
	}
	
	@RequestMapping(value = "/snippet/{id}", method = RequestMethod.GET)
	public String showSnippet(@PathVariable Long id, Model model){
		snippetRepository.findById(id).ifPresent(o -> model.addAttribute("snippet", o));
		System.out.println("Returning item:"+snippetRepository.findById(id).toString());
		return "snippet";
	}

}
