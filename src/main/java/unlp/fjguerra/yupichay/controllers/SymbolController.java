package unlp.fjguerra.yupichay.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lifia.lelparser.exceptions.InvalidRequirementFormatException;
import lifia.lelparser.model.UserStory;
import lifia.lelparser.model.parserResults.ParserResult;
import unlp.fjguerra.yupichay.domain.Project;

import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.ProjectRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;

@Controller
public class SymbolController {
	private SymbolRepository symbolRepository;
	
	@Autowired
	public void setSymbolRepository(SymbolRepository symbolRepository) {
		this.symbolRepository = symbolRepository;
	}
	
	@RequestMapping(value = "/symbols/getSymbols", method = RequestMethod.GET)
	public String getSymbols(Model model){
//		model.addAttribute("symbols", symbolRepository.findAll());
		model.addAttribute("symbols", symbolRepository.findByOrderByNotion());
		System.out.println("Returning symbols:");
		return "symbols";
	}
	
	@RequestMapping(value = "/symbol/{id}", method = RequestMethod.GET)
	public String showSymbol(@PathVariable Long id, Model model){
		symbolRepository.findById(id).ifPresent(o -> model.addAttribute("symbol", o));
		System.out.println("Returning symbol:");
		return "symbol";
	}
	

}
