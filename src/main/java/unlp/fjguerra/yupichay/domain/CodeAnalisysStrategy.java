package unlp.fjguerra.yupichay.domain;

import java.util.List;

import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;

public abstract class CodeAnalisysStrategy {

	CodeAnalisysStrategy() {

	}

	public abstract List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath);

	public abstract List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath);

	public abstract List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath);
	
	public abstract List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath,
			Item item);

	public abstract List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath,
			Item item);

	public abstract List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath,
			Item item);

//	public abstract void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository);
//
//	public abstract void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository);

	public abstract void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository);

	public abstract void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository);


}
