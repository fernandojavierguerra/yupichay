package unlp.fjguerra.yupichay.domain;

public class CodeFragment {
	
	private String fileName;
	private String target;
	private String typeTarget;
	private String insertString;
	private Boolean selected;
	private Integer lineNumber;
	
	
//	public CodeFragment(String fileName, String target, String typeTarget, String insertString) {
//		this.fileName = fileName;
//		this.target = target;
//		this.typeTarget = typeTarget;
//		this.insertString = insertString;
//		this.selected = true;
//	}
	
	public CodeFragment(String fileName, String target, String typeTarget, String insertString, Integer lineNumber) {
		this.fileName = fileName;
		this.target = target;
		this.typeTarget = typeTarget;
		this.insertString = insertString;
		this.selected = false;
		this.lineNumber = lineNumber;
	}
	
	public CodeFragment() {
	}

	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getTypeTarget() {
		return typeTarget;
	}
	public void setTypeTarget(String typeTarget) {
		this.typeTarget = typeTarget;
	}
	public String getInsertString() {
		return insertString;
	}
	public void setInsertString(String insertString) {
		this.insertString = insertString;
	}
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	@Override
	public String toString() {
		return "CodeFragment [fileName=" + fileName + ", target=" + target + ", typeTarget=" + typeTarget
				+ ", insertString=" + insertString + ", selected=" + selected + ", lineNumber=" + lineNumber + "]";
	}	
	

}
