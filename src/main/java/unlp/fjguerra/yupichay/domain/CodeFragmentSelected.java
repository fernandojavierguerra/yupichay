package unlp.fjguerra.yupichay.domain;

import java.util.List;

public class CodeFragmentSelected {
	
	private List<CodeFragment> codeFragments;

	public List<CodeFragment> getCodeFragments() {
		return codeFragments;
	}

	public void setCodeFragments(List<CodeFragment> snippets) {
		this.codeFragments = snippets;
	}

	@Override
	public String toString() {
		return "SnippetClassSelected [snippets=" + codeFragments + "]";
	}
	
}
