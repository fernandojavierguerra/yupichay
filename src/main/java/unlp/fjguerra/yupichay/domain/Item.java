package unlp.fjguerra.yupichay.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


/**
 * @author Fernando Javier Guerra <fernandojavierguerra@gmail.com>
 * 
 */

@Entity
public class Item {
	
	@Id
	private String id;
	private String text;
	@ManyToMany(fetch = FetchType.EAGER, mappedBy="items")
	private Set<Symbol> symbols = new HashSet<Symbol>();
	@ManyToOne
	private Project project; 
	
	@ManyToOne
	@JoinColumn(name = "FK_PARENT_ITEM")
	private Item parentItem;

	@OneToMany(mappedBy="parentItem", cascade = CascadeType.ALL)
	private Set<Item> subItems = new HashSet<Item>();
	
	private String location;
	
	@ManyToMany(fetch = FetchType.EAGER, mappedBy="items")
	private Set<Snippet> snippets = new HashSet<Snippet>();
	
	public Item() {
		
	}
	
	public Item(String id, String text) {
		this.id = id;
		this.text = text;
	}


	public Item(String id, String text, Project project) {
		this.id = id;
		this.text = text;
		this.project = project;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public Set<Symbol> getSymbols() {
		return symbols;
	}

	public void setSymbols(Set<Symbol> symbols) {
		this.symbols = symbols;
	}
	
	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void addSymbol(Symbol symbol) {
		this.symbols.add(symbol);
		symbol.getItems().add(this);
	}
	
	public void removeSymbol(Symbol symbol) {
		this.symbols.remove(symbol);
		symbol.getItems().remove(this);
	}
	

	public Item getParentItem() {
		return parentItem;
	}

	public void setParentItem(Item parentItem) {
		this.parentItem = parentItem;
	}

	public Set<Item> getSubItems() {
		return subItems;
	}

	public void setSubItems(Set<Item> subItems) {
		this.subItems = subItems;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Set<Snippet> getSnippets() {
		return snippets;
	}

	public void setSnippets(Set<Snippet> snippets) {
		this.snippets = snippets;
	}
	
	public void addSnippet(Snippet snippet) {
		this.snippets.add(snippet);
		snippet.getItems().add(this);
	}
	
	public void removeSnippet(Snippet snippet) {
		this.snippets.remove(snippet);
		snippet.getItems().remove(this);
	}
	
	@Override
	public String toString() {
		return "Item [id=" + id + ", text=" + text + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Item)) {
			return false;
		}
		Item other = (Item) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		return 31;
	}

}
