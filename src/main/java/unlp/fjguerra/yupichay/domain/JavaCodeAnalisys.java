package unlp.fjguerra.yupichay.domain;

import java.util.ArrayList;
import java.util.List;

import spoon.Launcher;
import unlp.fjguerra.yupichay.processor.java.AnnotationClassProcessor;
import unlp.fjguerra.yupichay.processor.java.AnnotationMethodProcessor;
import unlp.fjguerra.yupichay.processor.java.SnippetClassProcessor;
import unlp.fjguerra.yupichay.processor.java.SnippetMethodProcessor;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;

public class JavaCodeAnalisys extends CodeAnalisysStrategy {

	@Override
	public List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		Launcher spoon = new Launcher();
		spoon.addInputResource(inputPath);
		spoon.setSourceOutputDirectory("target/spooned/");
		AnnotationClassProcessor annotationClassProcessor = new AnnotationClassProcessor();
		annotationClassProcessor.setSymbols(symbols);
		spoon.addProcessor(annotationClassProcessor);
		spoon.run();
		fragments.addAll(annotationClassProcessor.getCodeFragments());

		Launcher spoon2 = new Launcher();
		spoon2.addInputResource(inputPath);
		spoon2.setSourceOutputDirectory("");
		AnnotationMethodProcessor annotationMethodProcessor = new AnnotationMethodProcessor();
		annotationMethodProcessor.setSymbols(symbols);
		spoon2.addProcessor(annotationMethodProcessor);
		spoon2.run();
		fragments.addAll(annotationMethodProcessor.getCodeFragments());

		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		Launcher spoon = new Launcher();
		spoon.addInputResource(inputPath);
		spoon.setSourceOutputDirectory("");
		AnnotationClassProcessor annotationClassProcessor = new AnnotationClassProcessor();
		annotationClassProcessor.setSymbols(symbols);
		spoon.addProcessor(annotationClassProcessor);
		spoon.run();
		fragments.addAll(annotationClassProcessor.getCodeFragments());

		Launcher spoon2 = new Launcher();
		spoon2.addInputResource(inputPath);
		spoon2.setSourceOutputDirectory("");
		AnnotationMethodProcessor annotationMethodProcessor = new AnnotationMethodProcessor();
		annotationMethodProcessor.setSymbols(symbols);
		spoon2.addProcessor(annotationMethodProcessor);
		spoon2.run();
		fragments.addAll(annotationMethodProcessor.getCodeFragments());

		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		Launcher spoon = new Launcher();
		spoon.addInputResource(inputPath);
		spoon.setSourceOutputDirectory("");
		AnnotationMethodProcessor annotationMethodProcessor = new AnnotationMethodProcessor();
		annotationMethodProcessor.setSymbols(symbols);
		spoon.addProcessor(annotationMethodProcessor);
		spoon.run();
		fragments.addAll(annotationMethodProcessor.getCodeFragments());
		return fragments;
	}

//	@Override
//	public void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository) {
//		Launcher spoon = new Launcher();
//		spoon.addInputResource(project.getInputPath());
//		spoon.setSourceOutputDirectory("");
//
//		SnippetClassProcessor snippetClassProcessor = new SnippetClassProcessor(project, symbolRepository,
//				snippetRepository);
//		spoon.addProcessor(snippetClassProcessor);
//		spoon.run();
//
//	}
//
//	@Override
//	public void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository) {
//		Launcher spoon = new Launcher();
//		spoon.addInputResource(project.getInputPath());
//		spoon.setSourceOutputDirectory("");
//
//		SnippetMethodProcessor snippetMethodProcessor = new SnippetMethodProcessor(project, symbolRepository,
//				snippetRepository);
//		spoon.addProcessor(snippetMethodProcessor);
//		spoon.run();
//
//	}
	
	@Override
	public void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		Launcher spoon = new Launcher();
		spoon.addInputResource(project.getInputPath());
		spoon.setSourceOutputDirectory("");

		SnippetClassProcessor snippetClassProcessor = new SnippetClassProcessor(project, symbolRepository,
				snippetRepository, itemRepository);
		spoon.addProcessor(snippetClassProcessor);
		spoon.run();

	}

	@Override
	public void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		Launcher spoon = new Launcher();
		spoon.addInputResource(project.getInputPath());
		spoon.setSourceOutputDirectory("");

		SnippetMethodProcessor snippetMethodProcessor = new SnippetMethodProcessor(project, symbolRepository,
				snippetRepository, itemRepository);
		spoon.addProcessor(snippetMethodProcessor);
		spoon.run();
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath, Item item) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		Launcher spoon = new Launcher();
		spoon.addInputResource(inputPath);
		spoon.setSourceOutputDirectory("target/spooned/");
		AnnotationClassProcessor annotationClassProcessor = new AnnotationClassProcessor();
		annotationClassProcessor.setSymbols(symbols);
		annotationClassProcessor.setItem(item);
		spoon.addProcessor(annotationClassProcessor);
		spoon.run();
		fragments.addAll(annotationClassProcessor.getCodeFragments());

		Launcher spoon2 = new Launcher();
		spoon2.addInputResource(inputPath);
		spoon2.setSourceOutputDirectory("");
		AnnotationMethodProcessor annotationMethodProcessor = new AnnotationMethodProcessor();
		annotationMethodProcessor.setSymbols(symbols);
		annotationMethodProcessor.setItem(item);
		spoon2.addProcessor(annotationMethodProcessor);
		spoon2.run();
		fragments.addAll(annotationMethodProcessor.getCodeFragments());

		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath, Item item) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		Launcher spoon = new Launcher();
		spoon.addInputResource(inputPath);
		spoon.setSourceOutputDirectory("target/spooned/");
		AnnotationClassProcessor annotationClassProcessor = new AnnotationClassProcessor();
		annotationClassProcessor.setSymbols(symbols);
		if (item != null)
			annotationClassProcessor.setItem(item);
		spoon.addProcessor(annotationClassProcessor);
		spoon.run();
		fragments.addAll(annotationClassProcessor.getCodeFragments());

		Launcher spoon2 = new Launcher();
		spoon2.addInputResource(inputPath);
		spoon2.setSourceOutputDirectory("");
		AnnotationMethodProcessor annotationMethodProcessor = new AnnotationMethodProcessor();
		annotationMethodProcessor.setSymbols(symbols);
		if (item != null)
			annotationMethodProcessor.setItem(item);
		spoon2.addProcessor(annotationMethodProcessor);
		spoon2.run();
		fragments.addAll(annotationMethodProcessor.getCodeFragments());

		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath, Item item) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		Launcher spoon = new Launcher();
		spoon.addInputResource(inputPath);
		spoon.setSourceOutputDirectory("");
		AnnotationMethodProcessor annotationMethodProcessor = new AnnotationMethodProcessor();
		annotationMethodProcessor.setSymbols(symbols);
		if (item != null)
			annotationMethodProcessor.setItem(item);
		spoon.addProcessor(annotationMethodProcessor);
		spoon.run();
		fragments.addAll(annotationMethodProcessor.getCodeFragments());
		return fragments;
	}



}
