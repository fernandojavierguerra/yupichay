package unlp.fjguerra.yupichay.domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.thm.mni.ii.phpparser.PHPParser;
import de.thm.mni.ii.phpparser.ast.Basic;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;

public class OtherCodeAnalisys extends CodeAnalisysStrategy {

	private List<CodeFragment> codeFragments = new ArrayList<CodeFragment>();
	
	public OtherCodeAnalisys() {
	}

	public List<CodeFragment> getCodeFragments() {
		return codeFragments;
	}

	public void setCodeFragments(List<CodeFragment> codeFragments) {
		this.codeFragments = codeFragments;
	}
	
	public void addSnippet(CodeFragment codeFragment) {
		this.codeFragments.add(codeFragment);
	} 
	
	private List<String> getProjectFilesPath(String inputPath) {
		Path projectPath = Paths.get(inputPath).toAbsolutePath();
//		try (Stream<Path> paths = Files.walk(Paths.get(inputPath).toAbsolutePath()).filter(Files::isRegularFile)) {
		try (Stream<Path> paths = Files.walk(projectPath).filter(Files::isRegularFile)) {
	        List<String> list = paths
	        		.map(path -> path.toString())
	                .collect(Collectors.toList());
//	                .map(path -> Files.isDirectory(path) ? path.toString() + '/' : path.toString())

	        return list;
	    } catch (IOException e) {
	        e.printStackTrace();
	    }

		return null;
		
	}

	public List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath) {
		List<String> lista = getProjectFilesPath(inputPath);
//		System.out.println("list: " + lista.toString());
		PHPParser.Result res = (PHPParser.Result) PHPParser.parse("\n" + 
				"<?php\n" + 
				"\n" + 
				"// clase base con propiedades y métodos miembro\n" + 
				"class Verdura {\n" + 
				"\n" + 
				"   var $comestible;\n" + 
				"   var $color;\n" + 
				"\n" + 
				"   function Verdura($comestible, $color=\"verde\")\n" + 
				"   {\n" + 
				"       $this->comestible = $comestible;\n" + 
				"       $this->color = $color;\n" + 
				"   }\n" + 
				"\n" + 
				"   function es_comestible()\n" + 
				"   {\n" + 
				"       return $this->comestible;\n" + 
				"   }\n" + 
				"\n" + 
				"   function qué_color()\n" + 
				"   {\n" + 
				"       return $this->color;\n" + 
				"   }\n" + 
				"\n" + 
				"} // fin de la clase Verdura\n" + 
				"\n" + 
				"// ampliar la clase base\n" + 
				"class Espinaca extends Verdura {\n" + 
				"\n" + 
				"   var $concinada = false;\n" + 
				"\n" + 
				"   function Espinaca()\n" + 
				"   {\n" + 
				"       $this->Verdura(true, \"verde\");\n" + 
				"   }\n" + 
				"\n" + 
				"   function cocinarla()\n" + 
				"   {\n" + 
				"       $this->concinada = true;\n" + 
				"   }\n" + 
				"\n" + 
				"   function está_cocinada()\n" + 
				"   {\n" + 
				"       return $this->concinada;\n" + 
				"   }\n" + 
				"\n" + 
				"} // fin de la clase Espinaca\n" + 
				"\n" + 
				"?>\n" + 
				""
				+ "");
        if (res instanceof PHPParser.Success) {
            Basic.Script s = ((PHPParser.Success) res).script();
            System.out.println(s);
        } else if (res instanceof PHPParser.Failure) {
            String msg = ((PHPParser.Failure) res).fullMsg();
            System.out.println(msg);
        }
		
		for (Symbol s : symbols) {
			String traceAnnotation = this.generateAnnotation(s.getNotion(), s.getSymbolType());
//			CodeFragment snippetClass =  this.insertSnippetTo(element, traceAnnotation, element.getPosition().getLine());
			CodeFragment snippetClass = new CodeFragment();
			this.addSnippet(snippetClass);
		}
		return null;
	}


	
	public List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath) {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateAnnotation(String notion, String symbolType) {
		String annotation = "@unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
				+ symbolType + "\") ";
		return annotation;
	}

//	@Override
//	public void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository) {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath, Item item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath, Item item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath, Item item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		// TODO Auto-generated method stub
		
	}


}
