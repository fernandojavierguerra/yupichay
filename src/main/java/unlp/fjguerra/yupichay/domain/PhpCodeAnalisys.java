package unlp.fjguerra.yupichay.domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.thm.mni.ii.phpparser.PHPParser;
import de.thm.mni.ii.phpparser.ast.Basic;
import unlp.fjguerra.yupichay.processor.php.PhpAnnotationClassProcessor;
import unlp.fjguerra.yupichay.processor.php.PhpAnnotationMethodProcessor;
import unlp.fjguerra.yupichay.processor.php.PhpProcessor;
import unlp.fjguerra.yupichay.processor.php.PhpSnippetClassProcessor;
import unlp.fjguerra.yupichay.processor.php.PhpSnippetMethodProcessor;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

public class PhpCodeAnalisys extends CodeAnalisysStrategy {

	public PhpCodeAnalisys() {
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		PhpAnnotationClassProcessor classProcessor = new PhpAnnotationClassProcessor();
		PhpAnnotationMethodProcessor methodProcessor = new PhpAnnotationMethodProcessor();
		fragments.addAll(methodProcessor.process(symbols, inputPath, null));
		fragments.addAll(classProcessor.process(symbols, inputPath, null));
		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		PhpAnnotationClassProcessor classProcessor = new PhpAnnotationClassProcessor();
		PhpAnnotationMethodProcessor methodProcessor = new PhpAnnotationMethodProcessor();
		fragments.addAll(methodProcessor.process(symbols, inputPath, null));
		fragments.addAll(classProcessor.process(symbols, inputPath, null));
		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		PhpAnnotationMethodProcessor methodProcessor = new PhpAnnotationMethodProcessor();
		fragments.addAll(methodProcessor.process(symbols, inputPath, null));
		return fragments;
	}

//	@Override
//	public void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository) {
//		PhpProcessor phpSnippetClassProcessor = new PhpSnippetClassProcessor();
//		phpSnippetClassProcessor.findAnnotations(project, symbolRepository, snippetRepository);
//		
//	}
//
//	@Override
//	public void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
//			SnippetRepository snippetRepository) {
//		PhpProcessor phpSnippetMethodProcessor = new PhpSnippetMethodProcessor();
//		phpSnippetMethodProcessor.findAnnotations(project, symbolRepository, snippetRepository);
//		
//	}
	
	@Override
	public void findAnnotationsInClasses(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		PhpProcessor phpSnippetClassProcessor = new PhpSnippetClassProcessor();
		phpSnippetClassProcessor.findAnnotations(project, symbolRepository, snippetRepository, itemRepository);

	}

	@Override
	public void findAnnotationsInMethods(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		PhpProcessor phpSnippetMethodProcessor = new PhpSnippetMethodProcessor();
		phpSnippetMethodProcessor.findAnnotations(project, symbolRepository, snippetRepository, itemRepository);
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingObjects(List<Symbol> symbols, String inputPath, Item item) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		PhpAnnotationClassProcessor classProcessor = new PhpAnnotationClassProcessor();
		PhpAnnotationMethodProcessor methodProcessor = new PhpAnnotationMethodProcessor();
		fragments.addAll(methodProcessor.process(symbols, inputPath, item));
		fragments.addAll(classProcessor.process(symbols, inputPath, item));
		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingSubjects(List<Symbol> symbols, String inputPath, Item item) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		PhpAnnotationClassProcessor classProcessor = new PhpAnnotationClassProcessor();
		PhpAnnotationMethodProcessor methodProcessor = new PhpAnnotationMethodProcessor();
		fragments.addAll(methodProcessor.process(symbols, inputPath, item));
		fragments.addAll(classProcessor.process(symbols, inputPath, item));
		return fragments;
	}

	@Override
	public List<CodeFragment> analyzeCodeSearchingVerbs(List<Symbol> symbols, String inputPath, Item item) {
		List<CodeFragment> fragments = new ArrayList<CodeFragment>();
		PhpAnnotationMethodProcessor methodProcessor = new PhpAnnotationMethodProcessor();
		fragments.addAll(methodProcessor.process(symbols, inputPath, item));
		return fragments;
	}





}
