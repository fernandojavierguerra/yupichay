package unlp.fjguerra.yupichay.domain;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import lifia.lelparser.exceptions.InvalidRequirementFormatException;
import lifia.lelparser.model.NLParser;
import lifia.lelparser.model.UserStory;
import lifia.lelparser.model.parserResults.ParserResult;
import lifia.lelparser.model.phraseContent.PhraseContent;
import spoon.Launcher;
import spoon.reflect.code.CtCodeSnippetStatement;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import unlp.fjguerra.yupichay.processor.java.AnnotationClassProcessor;
import unlp.fjguerra.yupichay.processor.java.AnnotationMethodProcessor;
import unlp.fjguerra.yupichay.processor.java.SnippetClassProcessor;
import unlp.fjguerra.yupichay.processor.java.SnippetMethodProcessor;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

/**
 * @author Fernando Javier Guerra <fernandojavierguerra@gmail.com>
 *
 */

@Entity
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany(mappedBy = "project")
	private Set<Item> items;
	@OneToMany(mappedBy = "project")
	private Set<Symbol> symbols;
	@OneToMany(mappedBy = "project")
	private Set<Snippet> snippets;

	private String inputPath;
	private String outputPath;

	private String programmingLanguage;
	private String itemsPath;

	public Project() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public Set<Symbol> getSymbols() {
		return symbols;
	}

	public void setSymbols(Set<Symbol> symbols) {
		this.symbols = symbols;
	}

	public void addSymbol(Symbol symbol) {
		this.symbols.add(symbol);
	}

	public Set<Snippet> getSnippets() {
		return snippets;
	}

	public void setSnippets(Set<Snippet> snippets) {
		this.snippets = snippets;
	}

	public String getInputPath() {
		return inputPath;
	}

	public void setInputPath(String inputPath) {
		this.inputPath = inputPath;
	}

	public String getOutputPath() {
		return outputPath;
	}

	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public String getProgrammingLanguage() {
		return programmingLanguage;
	}

	public void setProgrammingLanguage(String programmingLanguage) {
		this.programmingLanguage = programmingLanguage;
	}
	
	public String getItemsPath() {
		return itemsPath;
	}

	public void setItemsPath(String itemsPath) {
		this.itemsPath = itemsPath;
	}

	@Override
	public String toString() {
		return "Project [items=" + items + ", symbols=" + symbols + "]";
	}

	public ParserResult parseItems(Collection<UserStory> userStories) throws InvalidRequirementFormatException {
		NLParser parser = new NLParser();
		ParserResult result = parser.parseBatchUserStories(userStories);
		return result;
	}

	/**
	 * <p>
	 * Obtener los símbolos que están en cada item (User Story)
	 * </p>
	 * 
	 * @param result
	 * @param symbolRepository
	 */
	public void getSymbolsFromResult(ParserResult result, SymbolRepository symbolRepository) {
		// Create symbols
		// Separate items and symbols
		for (PhraseContent currentContent : result.getContent()) {
			// Create symbol by type
			Symbol symbol = new Symbol(currentContent.getValue(), currentContent.getType(), this);
			symbolRepository.save(symbol);
		}
	}

	public Collection<UserStory> buildSampleUserStories() {
		ArrayList<UserStory> stories = new ArrayList<>();
		stories.add(new UserStory("US0",
				"This is EPIC"));
		stories.add(new UserStory("US1",
				"As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators"));
		stories.add(new UserStory("US2",
				"As a moderator I want to invite estimators by given them a url where they can access the game So that we can start the game"));
		stories.add(new UserStory("US3",
				"As an estimator I want to join a game by entering my name on the page I received the url for So that I can participate"));
		stories.add(new UserStory("US4",
				"As a moderator I want to start a round by entering an item in a single multi-line text field So that we can estimate it"));
		stories.add(new UserStory("US5",
				"As an estimator I want to see the item we are estimating So that we can estimate it"));
		stories.add(new UserStory("US6",
				"As a moderator I want to edit an item in the list of items to be estimated So that I can make it better reflect the team’s understanding of the item"));
		stories.add(new UserStory("US7",
				"As a moderator I want to export a transcript of a game as a CSV file So that I can further process the stories and estimates"));
		

		return stories;
	};

	public Collection<UserStory> buildOneUserStories() {
		ArrayList<UserStory> stories = new ArrayList<>();

		stories.add(new UserStory("US1",
				"As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators"));
		return stories;
	};

	private static List<UserStory> readResourceAsUserStorylines(String name) throws IOException, URISyntaxException {
		List<UserStory> result;
		URL resource = Project.class.getResource(name);
		System.out.println("********************  ***************");
		System.out.println(resource.toString());
		Stream<String> stream = Files.lines(Paths.get(resource.toURI()));
		AtomicInteger i = new AtomicInteger(0);

		try {

			result = stream.filter(s -> !s.startsWith("#")).filter(s -> !s.isEmpty()).map(line -> {
				return new UserStory("US" + i.incrementAndGet(), line);
			}).collect(Collectors.toList());
		} finally {
			stream.close();
		}
		return result;
	}

	public Collection<UserStory> readUS() throws IOException, URISyntaxException {

		String name = "/project/petclinic/us.txt";
		List<UserStory> result;
		result = readResourceAsUserStorylines(name);

		return result;
	}

	/**
	 * <p>
	 * Grabar los Items (User Stories) en la BBDD
	 * </p>
	 * 
	 * @param userStories
	 * @param itemRepository
	 */
	public void saveItems(Collection<UserStory> userStories, ItemRepository itemRepository) {
		for (UserStory currentUS : userStories) {
			Item item = new Item(currentUS.getId(), currentUS.getText(), this);
			itemRepository.save(item);
		}
	}

	/**
	 * <p>
	 * Vincular simbolo con el item (User Story) correspondiente
	 * </p>
	 * 
	 * @param result
	 * @param itemRepository
	 * @param symbolRepository
	 */
	public void linkItemsWithSymbols(ParserResult result, ItemRepository itemRepository,
			SymbolRepository symbolRepository) {

		for (PhraseContent currentContent : result.getContent()) {
//			System.out.println("********************  ***************");
//			System.out.println("Result:"+currentContent.toString());
			
			Symbol symbol = symbolRepository.findByNotionAndSymbolType(currentContent.getValue(),
					currentContent.getType());
			
//			System.out.println("********************  ***************");
//			System.out.println("Symbol:"+symbol.toString());
			
			currentContent.getUsReferences().stream().forEach((currUS) -> {
				Item item = itemRepository.findById(currUS.getId()).orElse(null);
				item.addSymbol(symbol);

			});
			symbolRepository.save(symbol);

		}
	}
	protected List<String> getProjectItemsPath(String inputPath) {
		Path projectPath = Paths.get(inputPath).toAbsolutePath();
		
		try (Stream<Path> paths = Files.walk(projectPath).filter(Files::isRegularFile)) {

			List<String> list = paths.map(path -> path.toString()).collect(Collectors.toList());

			return list;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}
	
	
	public Collection<UserStory> readItems() throws IOException, URISyntaxException {
		
		switch (this.getProgrammingLanguage()) {
		case "java":
			return this.readUS();
		case "php":
			return this.getSyliusItems();
		default:
			return null;
		}
		
	}
	
	
	public Collection<UserStory> getSyliusItems() throws IOException, URISyntaxException {
		
		List<String> fileList = this.getProjectItemsPath(this.getItemsPath());
		
		FileUtility fileUtility = new FileUtility();
		ArrayList<UserStory> result = new ArrayList<>();
		String featureRegex = "\\s*Feature";
		AtomicInteger i = new AtomicInteger(0);
		for (String file : fileList) {

			Integer lineNumber = fileUtility.getLineNumber(file, featureRegex);
			if (lineNumber > 0) {
				String stringFile = fileUtility.readFileToString(file);
				String asPart = fileUtility.getSourceCode(stringFile, lineNumber+1, lineNumber + 2);
				String iWantPart = fileUtility.getSourceCode(stringFile, lineNumber+2, lineNumber + 3);
				String soThatPart = fileUtility.getSourceCode(stringFile, lineNumber, lineNumber+1);
				soThatPart = soThatPart.replaceFirst("In order to", " so that I can ");
				String item = asPart.trim()+" " + iWantPart.trim() + " "+soThatPart.trim();
				//eliminar saltos de linea y reemplazar In order to por so that
				item = item.replaceAll(System.getProperty("line.separator"), "");
				UserStory us = new UserStory("US" + i.incrementAndGet(), item);
				result.add(us);		
				
			}
		}
		return result;
	}

	/**
	 * <p>
	 * Revisar el codigo en busca de simbolos, se busca en clases y metodos
	 * </p>
	 * 
	 * @param symbolRepository
	 * @param snippetRepository
	 */
//	public void getSnippetsFromCode(SymbolRepository symbolRepository, SnippetRepository snippetRepository) {
//		CodeAnalisysStrategy codeAnalisysStrategy;
//		switch (this.getProgrammingLanguage()) {
//		case "java":
//			codeAnalisysStrategy = new JavaCodeAnalisys();
//			break;
//		case "php":
//			codeAnalisysStrategy = new PhpCodeAnalisys();
//			break;
//		default:
//			codeAnalisysStrategy = new JavaCodeAnalisys();
//		}
//
//		codeAnalisysStrategy.findAnnotationsInClasses(this, symbolRepository, snippetRepository);
//		codeAnalisysStrategy.findAnnotationsInMethods(this, symbolRepository, snippetRepository);
//
//	}
	

	public void getSnippetsFromCode(SymbolRepository symbolRepository, SnippetRepository snippetRepository,
			ItemRepository itemRepository) {
		CodeAnalisysStrategy codeAnalisysStrategy;
		switch (this.getProgrammingLanguage()) {
		case "java":
			codeAnalisysStrategy = new JavaCodeAnalisys();
			break;
		case "php":
			codeAnalisysStrategy = new PhpCodeAnalisys();
			break;
		default:
			codeAnalisysStrategy = new JavaCodeAnalisys();
		}

		codeAnalisysStrategy.findAnnotationsInClasses(this, symbolRepository, snippetRepository, itemRepository);
		codeAnalisysStrategy.findAnnotationsInMethods(this, symbolRepository, snippetRepository, itemRepository);

		
	}

	public List<CodeFragment> annotateCodeWithObjects(SymbolRepository symbolRepository,
			SnippetRepository snippetRepository) {
		CodeAnalisysStrategy codeAnalisysStrategy;
		List<Symbol> symbols = symbolRepository.findBySymbolType("object");

		switch (this.getProgrammingLanguage()) {
		case "java":
			codeAnalisysStrategy = new JavaCodeAnalisys();
			break;
		case "php":
			codeAnalisysStrategy = new PhpCodeAnalisys();
			break;
		default:
			codeAnalisysStrategy = new JavaCodeAnalisys();
		}

		return codeAnalisysStrategy.analyzeCodeSearchingObjects(symbols, inputPath);

	}

	public List<CodeFragment> annotateCodeWithSubjects(SymbolRepository symbolRepository,
			SnippetRepository snippetRepository) {
		CodeAnalisysStrategy codeAnalisysStrategy;
		List<Symbol> symbols = symbolRepository.findBySymbolType("subject");

		switch (this.getProgrammingLanguage()) {
		case "java":
			codeAnalisysStrategy = new JavaCodeAnalisys();
			break;
		case "php":
			codeAnalisysStrategy = new PhpCodeAnalisys();
			break;
		default:
			codeAnalisysStrategy = new JavaCodeAnalisys();
		}
		return codeAnalisysStrategy.analyzeCodeSearchingSubjects(symbols, inputPath);

	}

	public List<CodeFragment> annotateCodeWithVerbs(SymbolRepository symbolRepository,
			SnippetRepository snippetRepository) {
		CodeAnalisysStrategy codeAnalisysStrategy;
		List<Symbol> symbols = symbolRepository.findBySymbolType("verb");

		switch (this.getProgrammingLanguage()) {
		case "java":
			codeAnalisysStrategy = new JavaCodeAnalisys();
			break;
		case "php":
			codeAnalisysStrategy = new PhpCodeAnalisys();
			break;
		default:
			codeAnalisysStrategy = new JavaCodeAnalisys();
		}
		return codeAnalisysStrategy.analyzeCodeSearchingVerbs(symbols, inputPath);
	}
	
	public List<CodeFragment> annotateCodeWithSymbol(SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, Long id) {
		CodeAnalisysStrategy codeAnalisysStrategy;
		List<Symbol> symbols = new ArrayList();
		Symbol symbol = symbolRepository.findById(id).orElse(null);
		symbols.add(symbol);

		switch (this.getProgrammingLanguage()) {
		case "java":
			codeAnalisysStrategy = new JavaCodeAnalisys();
			break;
		case "php":
			codeAnalisysStrategy = new PhpCodeAnalisys();
			break;
		default:
			codeAnalisysStrategy = new JavaCodeAnalisys();
		}
		
		List<CodeFragment> fragments = new ArrayList(); 
		
		switch (symbol.getSymbolType()) {
		case "object":
			fragments = codeAnalisysStrategy.analyzeCodeSearchingObjects(symbols, inputPath);
			break;
		case "subject":
			fragments = codeAnalisysStrategy.analyzeCodeSearchingSubjects(symbols, inputPath);
			break;
		case "verb":
			fragments = codeAnalisysStrategy.analyzeCodeSearchingVerbs(symbols, inputPath);
			break;
		}
		
		return fragments;
		
	}
	
	public List<CodeFragment> annotateCodeWithSymbolFromItem(SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository, Long symbolId, String itemId) {
		CodeAnalisysStrategy codeAnalisysStrategy;
		List<Symbol> symbols = new ArrayList();
		
		Item item = itemRepository.findById(itemId).orElse(null);
		String location = item.getLocation();
		
		if(location == null || location.isEmpty())
			location = inputPath;

		String[] locations = location.split(":", 0); 
		  
		Symbol symbol = symbolRepository.findById(symbolId).orElse(null);
		symbols.add(symbol);

		switch (this.getProgrammingLanguage()) {
		case "java":
			codeAnalisysStrategy = new JavaCodeAnalisys();
			break;
		case "php":
			codeAnalisysStrategy = new PhpCodeAnalisys();
			break;
		default:
			codeAnalisysStrategy = new JavaCodeAnalisys();
		}
		
		List<CodeFragment> fragments = new ArrayList(); 
		List<CodeFragment> newFragments = new ArrayList();
		
        for (String singleLocation : locations) { 
            System.out.println(singleLocation);
            
            switch (symbol.getSymbolType()) {
    		case "object":
    			newFragments = codeAnalisysStrategy.analyzeCodeSearchingObjects(symbols, singleLocation, item);
    			break;
    		case "subject":
    			newFragments = codeAnalisysStrategy.analyzeCodeSearchingSubjects(symbols, singleLocation, item);
    			break;
    		case "verb":
    			newFragments = codeAnalisysStrategy.analyzeCodeSearchingVerbs(symbols, singleLocation, item);
    			break;
    		}
            
            fragments.addAll(newFragments);
        }
		
		return fragments;
		
	}

	public void createEpicItems(ItemRepository itemRepository) {
		Item epicItem = itemRepository.findById("US0").orElse(null);
		Item item1 = itemRepository.findById("US1").orElse(null);
		Item item2 = itemRepository.findById("US3").orElse(null);
		Item item3 = itemRepository.findById("US4").orElse(null);
		item1.setParentItem(epicItem);
		item2.setParentItem(epicItem);
		item3.setParentItem(epicItem);
		itemRepository.save(item1);
		itemRepository.save(item2);
		itemRepository.save(item3);
		
	}




}
