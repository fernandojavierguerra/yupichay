package unlp.fjguerra.yupichay.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Snippet {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String qualifiedName;
	private String simpleName;
	private String position;
	@Column(length = 5000)
	private String sourceCode;
	private String fileName;
	@ManyToMany(fetch = FetchType.EAGER, mappedBy="snippets")
	private Set<Symbol> symbols = new HashSet<Symbol>();
	
	@ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	@JoinTable(name = "item_snippet",
		joinColumns = { @JoinColumn(name = "snippet_id") },
		inverseJoinColumns = { @JoinColumn(name = "item_id") })
	private Set<Item> items = new HashSet<Item>();
	
	@ManyToOne
	private Project project;

	
	public Snippet() {
	}

	public Snippet(String qualifiedName, String simpleName, String position, String sourceCode, Project project) {
		this.qualifiedName = qualifiedName;
		this.simpleName = simpleName;
		this.position = position;
		this.sourceCode = sourceCode;
		this.project = project;
		
	}
	
	public Snippet(String qualifiedName, String simpleName, String position, String sourceCode, Project project, String fileName) {
		this.qualifiedName = qualifiedName;
		this.simpleName = simpleName;
		this.position = position;
		this.sourceCode = sourceCode;
		this.project = project;
		this.fileName = fileName;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQualifiedName() {
		return qualifiedName;
	}

	public void setQualifiedName(String qualifiedName) {
		this.qualifiedName = qualifiedName;
	}

	public String getSimpleName() {
		return simpleName;
	}

	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Set<Symbol> getSymbols() {
		return symbols;
	}

	public void setSymbols(Set<Symbol> symbols) {
		this.symbols = symbols;
	}
	
	public void addSymbol(Symbol symbol) {
		this.symbols.add(symbol);
		symbol.getSnippets().add(this);
	}
	
	public void removeSymbol(Symbol symbol) {
		this.symbols.remove(symbol);
		symbol.getSnippets().remove(this);
	}
	
	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public void addItem(Item item) {
		this.items.add(item);
		item.getSnippets().add(this);
	}
	
	public void removeItem(Item item) {
		this.items.remove(item);
		item.getSnippets().remove(this);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Symbol)) {
			return false;
		}
		Snippet other = (Snippet) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		return 31;
	}
	

}
