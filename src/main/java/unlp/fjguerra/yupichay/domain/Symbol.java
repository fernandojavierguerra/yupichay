package unlp.fjguerra.yupichay.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 * @author Fernando Javier Guerra <fernandojavierguerra@gmail.com>
 * 
 */

@Entity
 public class Symbol {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String notion;
	private String symbolType;
	private String notionDetail;
	private String impact;
		
	@ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	@JoinTable(name = "symbol_item",
		joinColumns = { @JoinColumn(name = "symbol_id") },
		inverseJoinColumns = { @JoinColumn(name = "item_id") })
	private Set<Item> items = new HashSet<Item>();
	@ManyToOne
	private Project project;
	
	@ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	@JoinTable(name = "symbol_snippet",
			joinColumns = { @JoinColumn(name = "symbol_id") },
			inverseJoinColumns = { @JoinColumn(name = "snippet_id") })
	private Set<Snippet> snippets = new HashSet<Snippet>();
	
	public Symbol() {
	}
		

	public Symbol(String notion, String symboltype, Project project) {
		this.notion = notion;
		this.symbolType = symboltype;
		this.project = project;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotion() {
		return notion;
	}

	public void setNotion(String notion) {
		this.notion = notion;
	}

	public String getSymbolType() {
		return symbolType;
	}


	public void setSymbolType(String symboltype) {
		this.symbolType = symboltype;
	}
	
	
	public String getNotionDetail() {
		return notionDetail;
	}


	public void setNotionDetail(String notionDetail) {
		this.notionDetail = notionDetail;
	}


	public String getImpact() {
		return impact;
	}


	public void setImpact(String impact) {
		this.impact = impact;
	}


	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public Project getProject() {
		return project;
	}


	public void setProject(Project project) {
		this.project = project;
	}

	public void addItem(Item item) {
		this.items.add(item);
		item.getSymbols().add(this);
	}
	
	public void removeItem(Item item) {
		this.items.remove(item);
		item.getSymbols().remove(this);
	}
	
	public Set<Snippet> getSnippets() {
		return snippets;
	}


	public void setSnippets(Set<Snippet> snippets) {
		this.snippets = snippets;
	}
	
	public void addSnippet(Snippet snippet) {
		this.snippets.add(snippet);
		snippet.getSymbols().add(this);
	}
	
	public void removeSnippet(Snippet snippet) {
		this.snippets.add(snippet);
		snippet.getSymbols().remove(this);
	}


	@Override
	public String toString() {
		return "Symbol [id=" + id + ", notion=" + notion + ", SymbolType=" + symbolType + "]";
		//return "Symbol [id=" + id + ", notion=" + notion + ", SymbolType=" + SymbolType + ", project=" + project + "]";
		//return "Symbol [id=" + id + ", notion=" + notion + ", SymbolType=" + SymbolType + ", items=" + items + ", project=" + project + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Symbol)) {
			return false;
		}
		Symbol other = (Symbol) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	

}
