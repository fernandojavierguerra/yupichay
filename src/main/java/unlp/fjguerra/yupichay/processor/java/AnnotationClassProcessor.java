package unlp.fjguerra.yupichay.processor.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtPackageReference;
import spoon.reflect.reference.CtTypeReference;
import unlp.fjguerra.yupichay.domain.CodeFragment;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.util.FileUtility;

public class AnnotationClassProcessor extends AbstractProcessor<CtType> {

	private List<Symbol> symbols;

	private List<CodeFragment> codeFragments = new ArrayList<CodeFragment>();
	
	private Item item;

	public List<Symbol> getSymbols() {
		return symbols;
	}

	public void setSymbols(List<Symbol> symbols) {
		this.symbols = symbols;
	}

	public List<CodeFragment> getCodeFragments() {
		return codeFragments;
	}

	public void setCodeFragments(List<CodeFragment> codeFragments) {
		this.codeFragments = codeFragments;
	}
	
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public void addSnippet(CodeFragment codeFragment) {
		this.codeFragments.add(codeFragment);
	}

	public boolean annotationContainsSymbol(CtType element, Symbol s) {
		List<CtAnnotation<?>> annotations = element.getAnnotations();
		for (CtAnnotation a : annotations) {
			if (a.getAnnotationType().getSimpleName().equals("Trace")) {
				String notion = a.getValueAsString("notion");
				String symbolType = a.getValueAsString("symboltype");

				if (notion.equals(s.getNotion()) && symbolType.equals(s.getSymbolType())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean getInputFromUser(CtType element) {
		Scanner reader = new Scanner(System.in); // Reading from System.in

		try {
			System.out.println("Add annotation to " + element.getQualifiedName() + " (y/n)?: ");
			String s = reader.next();
			if (s.equalsIgnoreCase("y")) {
				return true;
			}
			return false;
		} finally {
			reader.close();
		}

	};

//	public String generateAnnotation(String notion, String symbolType) {
//		String annotation = "@unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
//				+ symbolType + "\") ";
//		return annotation;
//	}
	
	public String generateAnnotation(String notion, String symbolType) {
		String annotation = "";
//		String itemId = this.getItem().getId();
//		if (itemId == null || itemId.isEmpty())
		if (this.getItem() == null)	
			annotation = "@unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
				+ symbolType + "\") ";
		else
			annotation = "@unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
					+ symbolType + "\", item = \"" + this.getItem().getId() +"\") ";
		return annotation;
	}

	public CtAnnotation createTraceAnnotation(String simpleName, String refImport) {

		String className = CtType.class.getCanonicalName();

		CtAnnotation traceAnnotation = getFactory().Core().createAnnotation();
		CtTypeReference<Object> ref = getFactory().Core().createTypeReference();
		ref.setSimpleName(simpleName);

		CtPackageReference refPackage = getFactory().Core().createPackageReference();
		refPackage.setSimpleName(refImport);
		ref.setPackage(refPackage);
		traceAnnotation.setAnnotationType(ref);

		return traceAnnotation;
	}

	public CodeFragment insertSnippetTo(CtType element, String snippet, Integer lineNumber) {
		System.out.println("Qualified name: " + element.getQualifiedName().toString());
		String fileName = element.getPosition().getFile().toString();
		String target = element.getSimpleName();
		String typeTarget = "class";
		String insertString = snippet;

		return new CodeFragment(fileName, target, typeTarget, insertString, lineNumber);

	}

	@Override
	public void process(CtType element) {

		if (element instanceof CtClass) {
			for (Symbol s : symbols) {
				String simpleName = element.getSimpleName().toLowerCase();

				if (simpleName.contains(s.getNotion())) {
					if (!this.annotationContainsSymbol(element, s)) {

						String traceAnnotation = this.generateAnnotation(s.getNotion(), s.getSymbolType());
						CodeFragment snippetClass = this.insertSnippetTo(element, traceAnnotation,
								element.getPosition().getLine());
						this.addSnippet(snippetClass);
					}
				}
			}
		}
	}

}
