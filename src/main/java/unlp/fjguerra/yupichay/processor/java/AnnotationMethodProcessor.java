package unlp.fjguerra.yupichay.processor.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtPackageReference;
import spoon.reflect.reference.CtTypeReference;
import unlp.fjguerra.yupichay.domain.CodeFragment;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.util.FileUtility;

public class AnnotationMethodProcessor extends AbstractProcessor<CtType> {

	private List<Symbol> symbols;
	
	private List<CodeFragment> codeFragments = new ArrayList<CodeFragment>();
	
	private Item item;

	public List<Symbol> getSymbols() {
		return symbols;
	}
	
	public List<CodeFragment> getCodeFragments() {
		return codeFragments;
	}

	public void setCodeFragments(List<CodeFragment> codeFragments) {
		this.codeFragments = codeFragments;
	}

	public void setSymbols(List<Symbol> symbols) {
		this.symbols = symbols;
	}
	
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public void addSnippet(CodeFragment codeFragment) {
		this.codeFragments.add(codeFragment);
	}

	public boolean annotationContainsSymbol(CtMethod method, Symbol s) {
		List<CtAnnotation<?>> annotations = method.getAnnotations();
		for (CtAnnotation a : annotations) {
			if (a.getAnnotationType().getSimpleName().equals("Trace")) {
				String notion = a.getValueAsString("notion");
				String symbolType = a.getValueAsString("symboltype");

				if (notion.equals(s.getNotion()) && symbolType.equals(s.getSymbolType())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean getInputFromUser(CtType element) {
		Scanner reader = new Scanner(System.in); // Reading from System.in

		try {
			System.out.println("Add annotation to " + element.getQualifiedName() + " (y/n)?: ");
			String s = reader.next();
			if (s.equalsIgnoreCase("y")) {
				return true;
			}
			return false;
		} finally {
			reader.close();
		}

	};

//	public String generateAnnotation(String notion, String symbolType) {
//		String annotation = "@unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
//				+ symbolType + "\") ";
//		return annotation;
//	}

	public CtAnnotation createTraceAnnotation(String simpleName, String refImport) {

		String className = CtType.class.getCanonicalName();

		CtAnnotation traceAnnotation = getFactory().Core().createAnnotation();
		CtTypeReference<Object> ref = getFactory().Core().createTypeReference();
		ref.setSimpleName(simpleName);

		CtPackageReference refPackage = getFactory().Core().createPackageReference();
		refPackage.setSimpleName(refImport);
		ref.setPackage(refPackage);
		traceAnnotation.setAnnotationType(ref);

		return traceAnnotation;
	}

	public void insertSnippet(CtType element, String snippet, CtMethod method) {
		System.out.println("Class Qualified name: " + element.getQualifiedName().toString());
		System.out.println("Method signature: " + method.getSignature());

		String fileName = element.getPosition().getFile().toString();
		String target = method.getSimpleName();
		String typeTarget = ""; //no needed
		String insertString = snippet;

		FileUtility fileRelpace = new FileUtility();

		//fileRelpace.insertBefore(target, insertString, fileName, typeTarget);

	}
	
	public String generateAnnotation(String notion, String symbolType) {
		String annotation = "";
//		String itemId = this.getItem().getId();
//		if (itemId == null || itemId.isEmpty())
		if (this.getItem() == null)
			annotation = "@unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
				+ symbolType + "\") ";
		else
			annotation = "@unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
					+ symbolType + "\", item = \"" + this.getItem().getId() +"\") ";
		return annotation;
	}
	
	public CodeFragment insertSnippetTo(CtType element, String snippet, CtMethod method, Integer lineNumber) {
		System.out.println("Class Qualified name: " + element.getQualifiedName().toString());
		System.out.println("Method signature: " + method.getSignature());

		String fileName = element.getPosition().getFile().toString();
		String target = method.getSimpleName();
		String typeTarget = "method"; 
		String insertString = snippet;

		return new CodeFragment(fileName, target, typeTarget, insertString, lineNumber);
	}

	@Override
	public void process(CtType element) {
		if (element instanceof CtClass) {
			Set<CtMethod> methods = element.getMethods();
			for (CtMethod method : methods) {
				String simpleName = method.getSimpleName().toLowerCase();
				if (!simpleName.startsWith("set") && !simpleName.startsWith("get")) {
					for (Symbol s : symbols) {
						if (simpleName.contains(s.getNotion())) {
							if (!this.annotationContainsSymbol(method, s)) {

								String traceAnnotation = this.generateAnnotation(s.getNotion(), s.getSymbolType());
								CodeFragment snippetClass = this.insertSnippetTo(element, traceAnnotation, method, method.getPosition().getLine());
								this.addSnippet(snippetClass);
							}
						}
					}

				}

			}

		}
	}



}
