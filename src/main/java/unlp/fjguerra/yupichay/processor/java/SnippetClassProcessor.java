package unlp.fjguerra.yupichay.processor.java;

import java.util.List;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtType;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.domain.Snippet;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

public class SnippetClassProcessor extends AbstractProcessor<CtType> {

	SymbolRepository symbolRepository;
	SnippetRepository snippetRepository;
	ItemRepository itemRepository;
	Project project;

	public SnippetClassProcessor(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		this.project = project;
		this.symbolRepository = symbolRepository;
		this.snippetRepository = snippetRepository;
		this.itemRepository = itemRepository;
	}

	@Override
	public void process(CtType element) {
		if (element instanceof CtClass) {
			// TODO: Revisar esto
		}
		FileUtility utility = new FileUtility();
//	System.out.println("Clase: " + type.getSimpleName());
		List<CtAnnotation<?>> annotations = element.getAnnotations();
		for (CtAnnotation a : annotations) {
			if (a.getAnnotationType().getSimpleName().equals("Trace")) {

				String notion = a.getValueAsString("notion");
				String symbolType = a.getValueAsString("symboltype");

				Symbol symbol = symbolRepository.findByNotionAndSymbolType(notion, symbolType);

//				String source = utility.getSourceCode(element.toString(), element.getPosition().getLine() - 5, element.getPosition().getLine() + 5);
				String fileName =  element.getPosition().getFile().toString();
				String source = utility.getLines(fileName, element.getPosition().getLine());

				Snippet snippet = snippetRepository.findByQualifiedName(element.getQualifiedName().toString())
						.orElse(new Snippet(element.getQualifiedName().toString(), element.getSimpleName().toString(),
								element.getPosition().toString(), source, project, fileName));
				// Asociar snippet y anotacion
				snippetRepository.save(snippet);
				symbol.addSnippet(snippet);
				symbolRepository.save(symbol);
				
				//Asociar item y snippet
				String itemId = a.getValueAsString("item");
				
				if (itemId != null && !itemId.isEmpty() ) {
					Item item = itemRepository.findById(itemId).orElse(null);
					item.addSnippet(snippet);
					itemRepository.save(item);
				}

			}

		}

	}

}
