package unlp.fjguerra.yupichay.processor.java;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.domain.Snippet;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

public class SnippetMethodProcessor extends AbstractProcessor<CtType> {

	SymbolRepository symbolRepository;
	SnippetRepository snippetRepository;
	ItemRepository itemRepository;
	Project project;

	public SnippetMethodProcessor(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		this.project = project;
		this.symbolRepository = symbolRepository;
		this.snippetRepository = snippetRepository;
		this.itemRepository = itemRepository;
	}

	@Override
	public void process(CtType element) {
		FileUtility utility = new FileUtility();
		if (element instanceof CtClass) {
			Set<CtMethod> methods = element.getMethods();
//			System.out.println("Clase: " + element.getSimpleName());
			for (CtMethod method : methods) {
				List<CtAnnotation<?>> annotations = method.getAnnotations();

				for (CtAnnotation a : annotations) {
					if (a.getAnnotationType().getSimpleName().equals("Trace")) {

						String notion = a.getValueAsString("notion");
						String symbolType = a.getValueAsString("symboltype");

						Symbol symbol = symbolRepository.findByNotionAndSymbolType(notion, symbolType);

//						String source = utility.getSourceCode(element.toString(), element.getPosition().getLine() - 5, element.getPosition().getLine() + 5);
						String fileName =  element.getPosition().getFile().toString();
						String source = utility.getLines(fileName, element.getPosition().getLine());

						Snippet snippet = snippetRepository.findByQualifiedName(element.getQualifiedName().toString())
								.orElse(new Snippet(element.getQualifiedName().toString(),
										element.getSimpleName().toString(), element.getPosition().toString(), source,
										project, fileName));
						// Asociar snippet y anotacion
						snippetRepository.save(snippet);
						symbol.addSnippet(snippet);
						symbolRepository.save(symbol);
						
						//Asociar item y snippet
						String itemId = a.getValueAsString("item");
						System.out.println("ItemID: "+ itemId);
						if (itemId != null && !itemId.isEmpty() ) {
							Item item = itemRepository.findById(itemId).orElse(null);
							System.out.println("Item: "+ item.toString());
							item.addSnippet(snippet);
							itemRepository.save(item);
						}

					}
				}
			}

		}
	}

}
