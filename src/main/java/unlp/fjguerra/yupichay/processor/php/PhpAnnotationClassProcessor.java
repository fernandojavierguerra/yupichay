package unlp.fjguerra.yupichay.processor.php;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.thm.mni.ii.phpparser.PHPParser;
import de.thm.mni.ii.phpparser.ast.Basic;
import unlp.fjguerra.yupichay.domain.CodeFragment;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.util.FileUtility;

public class PhpAnnotationClassProcessor extends PhpProcessor {

	public PhpAnnotationClassProcessor() {
	}

	protected List<Symbol> symbols;

	private List<CodeFragment> codeFragments = new ArrayList<CodeFragment>();

	public List<CodeFragment> getCodeFragments() {
		return codeFragments;
	}

	public void setCodeFragments(List<CodeFragment> codeFragments) {
		this.codeFragments = codeFragments;
	}

	public void addSnippet(CodeFragment codeFragment) {
		this.codeFragments.add(codeFragment);
	}

	public List<Symbol> getSymbols() {
		return symbols;
	}

	public void setSymbols(List<Symbol> symbols) {
		this.symbols = symbols;
	}

	/**
	 * 
	 * Process to Annotate a Class
	 * 
	 * @param symbols
	 * @param inputPath Source Code Path
	 * @return
	 */
	public List<CodeFragment> process(List<Symbol> symbols, String inputPath, Item item) {
		List<String> fileList = this.getProjectFilesPath(inputPath);
		List<String> classList = new ArrayList<String>();
		FileUtility fileUtility = new FileUtility();
//		String classRegex = "\\s*class ";
//		String regexClass = "class*\\s+([a-zA-Z0-9_]+)";
		String regexClass = "\\w*\\s*class*\\s+([a-zA-Z0-9_]+)";
		

		for (String file : fileList) {
//			System.out.println("File: " + file);
			try {
				// buscar cada simbolo

				for (Symbol symbol : symbols) {

					Integer classLineNumber = fileUtility.getLineNumber(file, regexClass);
					if (classLineNumber > 0) {

						String classDeclaration = fileUtility.getLine(file, classLineNumber);
						
						String className = this.getElementName(classDeclaration, regexClass);

//						System.out.println("    Class name: " + className + "    Symbol: " + symbol.toString());
						if (className.toLowerCase().contains(symbol.getNotion().toLowerCase())) {
							String regexAnnotation = "\\s*@unlp.fjguerra.yupichay.annotations.Trace.*notion.*"
									+ symbol.getNotion() + ".*symboltype.*" + symbol.getSymbolType();
							Integer annotationLineNumber = fileUtility.getLineNumber(file, regexAnnotation);
//							System.out.println("    Symbol: " + symbol.toString() + "    Class name: " + className);
							if (annotationLineNumber < 0) {
								String traceAnnotation = this.generateAnnotation(symbol.getNotion(),
										symbol.getSymbolType(), item.getId());
//								System.out.println("    Annotation: " + traceAnnotation);
								CodeFragment snippetClass = this.insertSnippetTo(file, className, "class",
										traceAnnotation, classLineNumber);
								this.addSnippet(snippetClass);
							}
						}

					}

//					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return this.getCodeFragments();

	}

}
