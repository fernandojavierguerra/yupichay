package unlp.fjguerra.yupichay.processor.php;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import unlp.fjguerra.yupichay.domain.CodeFragment;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.util.FileUtility;

public class PhpAnnotationMethodProcessor extends PhpProcessor {

	public PhpAnnotationMethodProcessor() {
	}

	protected List<Symbol> symbols;

	private List<CodeFragment> codeFragments = new ArrayList<CodeFragment>();

	public List<CodeFragment> getCodeFragments() {
		return codeFragments;
	}

	public void setCodeFragments(List<CodeFragment> codeFragments) {
		this.codeFragments = codeFragments;
	}

	public void addSnippet(CodeFragment codeFragment) {
		this.codeFragments.add(codeFragment);
	}

	public List<Symbol> getSymbols() {
		return symbols;
	}

	public void setSymbols(List<Symbol> symbols) {
		this.symbols = symbols;
	}

	/**
	 * 
	 * Process to Annotate Methods
	 * 
	 * @param symbols
	 * @param inputPath Source Code Path
	 * @param item 
	 * @return
	 */
	public List<CodeFragment> process(List<Symbol> symbols, String inputPath, Item item) {
		List<String> fileList = this.getProjectFilesPath(inputPath);
		List<String> methodList = new ArrayList<String>();
		FileUtility fileUtility = new FileUtility();
		Map<Integer, String> methods = new HashMap<Integer, String>();

		for (String file : fileList) {
//			System.out.println("File: " + file);
			try {
//				String stringFile = fileUtility.readFileToString(file);
//				String astFile = this.fileToAST(stringFile);
//				methods = this.getAllMethodsName(file);
				String regexMethods = "function*\\s+([a-zA-Z0-9_]+)";
				methods = fileUtility.getAllElementsName(file, regexMethods);

				for (Symbol symbol : symbols) {

					for (Entry<Integer, String> method : methods.entrySet()) {
						Integer methodLineNumber = method.getKey();
						String methodName = method.getValue();
						if (methodName.toLowerCase().contains(symbol.getNotion().toLowerCase())) {

							String regexAnnotation = "\\s*@unlp.fjguerra.yupichay.annotations.Trace.*notion.*"
									+ symbol.getNotion() + ".*symboltype.*" + symbol.getSymbolType();
							//TODO revisar caso en que a veces esta tambien el item como parte de la anotacion
							Integer startPos = methodLineNumber - 2;
							Integer annotationLineNumber = fileUtility.getLineNumber(file, regexAnnotation, startPos);

							if (annotationLineNumber < 0) {
								String traceAnnotation = this.generateAnnotation(symbol.getNotion(),
										symbol.getSymbolType(), item.getId());
								CodeFragment snippetClass = this.insertSnippetTo(file, methodName, "method",
										traceAnnotation, methodLineNumber);
								this.addSnippet(snippetClass);
							}

						}
					}

					// buscar en metodos
//					System.out.println("    AST File: " + astFile);
//					methodList = this.getMethodsName(astFile, symbol.getNotion());
//
//					for (String method : methodList) {
//						String methodRegex = "\\s*function.*" + method;
//						Integer methodLineNumber = fileUtility.getLineNumber(file, methodRegex);
//
//						System.out.println("    Method line number: " + methodLineNumber.toString());
//						if (methodLineNumber > 0) {
//							String regexAnnotation = "\\s*@unlp.fjguerra.yupichay.annotations.Trace.*notion.*"
//									+ symbol.getNotion() + ".*symboltype.*" + symbol.getSymbolType();
//							Integer annotationLineNumber = fileUtility.getLineNumber(file, regexAnnotation);
//							if (annotationLineNumber < 0) {
//								String traceAnnotation = this.generateAnnotation(symbol.getNotion(),
//										symbol.getSymbolType());
//								CodeFragment snippetClass = this.insertSnippetTo(file, method, "method",
//										traceAnnotation, methodLineNumber);
//								this.addSnippet(snippetClass);
//							}
//
//						}
//
//					}

				}

			} catch (IOException e) {
				e.printStackTrace();

			}

		}
		return this.getCodeFragments();

	}

	private Map<Integer, String> getAllMethodsName(String file) {
		Map<Integer, String> methods = new HashMap<Integer, String>();
		FileUtility fileUtility = new FileUtility();
		String methodRegex = "\\s*function ";
		Integer methodLineNumber = null;

		try {
			methodLineNumber = fileUtility.getLineNumber(file, methodRegex, 1);
			while (methodLineNumber > 0) {
				String methodDeclaration = fileUtility.getLine(file, methodLineNumber);
				//System.out.println("    Method declaration: " + methodDeclaration);
				Integer startPos = methodDeclaration.indexOf("function") + 8;
				Integer endPos = methodDeclaration.indexOf(" ", startPos);
				String methodName = methodDeclaration.substring(startPos, endPos);
				methods.put(methodLineNumber, methodName);

				methodLineNumber = fileUtility.getLineNumber(file, methodRegex, methodLineNumber + 1);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return methods;
	}

}
