package unlp.fjguerra.yupichay.processor.php;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.thm.mni.ii.phpparser.PHPParser;
import de.thm.mni.ii.phpparser.ast.Basic;
import unlp.fjguerra.yupichay.domain.CodeFragment;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

public class PhpProcessor {

	protected List<String> getProjectFilesPath(String inputPath) {
		Path projectPath = Paths.get(inputPath).toAbsolutePath();
		// try (Stream<Path> paths =
		// Files.walk(projectPath).filter(Files::isRegularFile).filter(f ->
		// f.endsWith(".php"))) {
//		try (Stream<Path> paths = Files.walk(projectPath).filter(Files::isRegularFile).filter(f ->f.endsWith(".php"))) {
//		try (Stream<Path> paths = Files.walk(projectPath).filter(f ->f.endsWith(".php"))) {
		try (Stream<Path> paths = Files.walk(projectPath).filter(Files::isRegularFile).filter(f -> f.toString().endsWith(".php"))) {

			List<String> list = paths.map(path -> path.toString()).collect(Collectors.toList());

			return list;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	protected CodeFragment insertSnippetTo(String fileName, String target, String typeTarget, String insertString,
			Integer lineNumber) {
		return new CodeFragment(fileName, target, typeTarget, insertString, lineNumber);
	}

	/**
	 * 
	 * Get all Methods from Source Code that matches with the symbol
	 * 
	 * @param classDeclaration Source Code
	 * @param symbol symbol
	 * @return List<String> That contains Methods Name
	 */
	protected List<String> getMethodsName(String classDeclaration, String symbol) {

		List<String> list = new ArrayList<String>();
		String findStr = "MethodDecl";
		String nameStr = "Name";
		String name = "";

		int lastIndex = 0;
		int count = 0;

		while (lastIndex != -1) {

			lastIndex = classDeclaration.indexOf(findStr, lastIndex);

			if (lastIndex != -1) {
				lastIndex = classDeclaration.indexOf(nameStr, lastIndex);
				name = classDeclaration.substring(classDeclaration.indexOf("(", lastIndex) + 1,
						classDeclaration.indexOf(")", lastIndex));
				if (name.toLowerCase().contains(symbol.toLowerCase())) {
					list.add(name);
					System.out.println("	Method Name: "+name);
				}
			}
		}

		return list;

	}

	/**
	 * 
	 * Get Class Name from Source Code that matches with the symbol
	 * 
	 * @param classDeclaration Source Code
	 * @param symbol symbol
	 * @return List<String> That contains Class Name
	 */
	protected List<String> getClassName(String classDeclaration, String symbol) {

		List<String> list = new ArrayList<String>();
		String findStr = "ClassDecl";
		String nameStr = "Name";
		String name = "";

		int lastIndex = 0;
		int count = 0;

		while (lastIndex != -1) {

			lastIndex = classDeclaration.indexOf(findStr, lastIndex);

			if (lastIndex != -1) {
				lastIndex = classDeclaration.indexOf(nameStr, lastIndex);
				name = classDeclaration.substring(classDeclaration.indexOf("(", lastIndex) + 1,
						classDeclaration.indexOf(")", lastIndex));
				if (name.toLowerCase().contains(symbol.toLowerCase())) {
					list.add(name);
				}
			}
		}

		return list;

	}

	protected String generateAnnotation(String notion, String symbolType, String itemId) {
		String annotation = "";
		if (itemId == null || itemId.isEmpty()) 
			annotation = "// @unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
				+ symbolType + "\") ";
		else
			annotation = "// @unlp.fjguerra.yupichay.annotations.Trace(notion = \"" + notion + "\", symboltype = \""
					+ symbolType + "\", item = \"" + itemId + "\") ";
		return annotation;
	}

	protected String fileToAST(String stringFile) {
		PHPParser.Result res = (PHPParser.Result) PHPParser.parse(stringFile);
		if (res instanceof PHPParser.Success) {
			Basic.Script s = ((PHPParser.Success) res).script();
			return s.toString();
			// Basic.Name n = ((PHPParser.Success) res).toString();
			// System.out.println(s);
			// System.out.println(((PHPParser.Success) res).toString());
		} else if (res instanceof PHPParser.Failure) {
			String msg = ((PHPParser.Failure) res).fullMsg();
			// System.out.println(msg);
			return msg;
		}
		return null;
	}

	public void findAnnotations(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
	}

	protected String getQualifiedName(String astFile) {
		FileUtility fileUtility = new FileUtility();
		String nameSpaceDef = fileUtility.getTextInsideDelimiters(astFile, "NamespaceDef");
		String preQualifiedName = fileUtility.getTextInsideDelimiters(nameSpaceDef, "QualifiedName");
		String lastName = preQualifiedName.substring(preQualifiedName.lastIndexOf("Name"));
		String qualifiedName = fileUtility.getTextInsideDelimiters(lastName, "Name");
		String arrayBuffer = fileUtility.getTextInsideDelimiters(preQualifiedName, "ArrayBuffer");
		qualifiedName = this.parseArrayBuffer(arrayBuffer) + "\\" + qualifiedName + "\\" + this.getSimpleName(astFile);
		return qualifiedName;
	}

	private String parseArrayBuffer(String arrayBuffer) {
		FileUtility fileUtility = new FileUtility();
		String sub = "Name";
		String temp = arrayBuffer.replace(sub, "");
		Integer occ = (arrayBuffer.length() - temp.length()) / sub.length();
		String output = "";
		for (int i = 1; i <= occ; i++) {
			output = "\\" + fileUtility.getTextInsideDelimiters(arrayBuffer, "Name");
			arrayBuffer = arrayBuffer.substring(arrayBuffer.indexOf(",") + 1);
		}
		return output;
	}

	protected String getSimpleName(String astFile) {
		FileUtility fileUtility = new FileUtility();
		String nameSpaceDef = fileUtility.getTextInsideDelimiters(astFile, "ClassDecl");
		String some = fileUtility.getTextInsideDelimiters(nameSpaceDef, "Some");
		return fileUtility.getTextInsideDelimiters(some, "Name");
	}

	protected String getNotion(String annotationCodeLine) {
		// @unlp.fjguerra.yupichay.annotations.Trace(notion = "owner", symboltype =
		// "object")
		Integer pos = annotationCodeLine.indexOf("\"") + 1;
		String subStr = annotationCodeLine.substring(pos, annotationCodeLine.indexOf("\"", pos));
		System.out.println("Notion: " + subStr);
		return subStr;

	}

	protected String getSymbolType(String annotationCodeLine) {
		// @unlp.fjguerra.yupichay.annotations.Trace(notion = "owner", symboltype =
		// "object")
		Integer pos = annotationCodeLine.indexOf("\"", annotationCodeLine.indexOf(",")) + 1;
		String subStr = annotationCodeLine.substring(pos, annotationCodeLine.indexOf("\"", pos));
		System.out.println("SymbolType: " + subStr);
		return subStr;
	}
	
	protected String getItemId(String annotationCodeLine) {
		// @unlp.fjguerra.yupichay.annotations.Trace(notion = "owner", symboltype ="object", item = "US10")
		Integer pos0 = annotationCodeLine.indexOf(",") + 1;
		Integer pos = annotationCodeLine.indexOf("\"", annotationCodeLine.indexOf(",", pos0)) + 1;
		String subStr = annotationCodeLine.substring(pos, annotationCodeLine.indexOf("\"", pos));
		System.out.println("ItemId: " + subStr);
		return subStr;
	}
	
	protected String getElementName(String stringToBeMatched, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(stringToBeMatched);
		if (matcher.find())
        	return matcher.group(1);
        return null;
	}

}
