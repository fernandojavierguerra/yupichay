package unlp.fjguerra.yupichay.processor.php;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.domain.Snippet;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

public class PhpSnippetClassProcessor extends PhpProcessor {

	public PhpSnippetClassProcessor() {
	}

	@Override
	public void findAnnotations(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {

		List<String> fileList = this.getProjectFilesPath(project.getInputPath());
		FileUtility fileUtility = new FileUtility();
//
		String regexAnnotation = "@unlp.fjguerra.yupichay.annotations.Trace.*notion.*symboltype.*";
		String regexClass = "class*\\s+([a-zA-Z0-9_]+)";
		String regexNamespace = "namespace*\\s+([a-zA-Z\\\\]+)";
		Item item;
		for (String file : fileList) {
			try {
//				System.out.println("File: " + file);
				Integer annotationLineNumber = fileUtility.getLineNumber(file, regexAnnotation);
				Integer classLineNumber = fileUtility.getLineNumber(file, regexClass);
				if (annotationLineNumber > 0 && classLineNumber > 0 && annotationLineNumber < classLineNumber) {

					String classDeclaration = fileUtility.getLine(file, classLineNumber);
					String simpleName = this.getElementName(classDeclaration, regexClass);

//					System.out.println("File: " + file);
//					System.out.println("Annotation line: " + annotationLineNumber);
					Integer namespaceLineNumber = fileUtility.getLineNumber(file, regexNamespace);
					String namespaceDeclaration = fileUtility.getLine(file, namespaceLineNumber);
					String qualifiedName = this.getElementName(namespaceDeclaration, regexNamespace) + "\\" + simpleName;
//					System.out.println("QualiName: " + qualifiedName);
//					System.out.println("SimpleName: " + simpleName);

					String annotationCodeLine = fileUtility.getLine(file, annotationLineNumber);
//					System.out.println("Annotation: " + annotationCodeLine);
					String notion = this.getNotion(annotationCodeLine);
					String symbolType = this.getSymbolType(annotationCodeLine);

					Symbol symbol = symbolRepository.findByNotionAndSymbolType(notion, symbolType);

					String source = fileUtility.getLines(file, annotationLineNumber);
					Snippet snippet = snippetRepository.findByQualifiedName(qualifiedName).orElse(
							new Snippet(qualifiedName, simpleName, annotationLineNumber.toString(), source, project, file));
					// Asociar snippet y anotacion
					snippetRepository.save(snippet);
					symbol.addSnippet(snippet);
					symbolRepository.save(symbol);
					
					//El item no siempre esta en la anotacion
					String itemId = this.getItemId(annotationCodeLine);
					//Si es no nulo buscar item en BD
					
					if (!itemId.isEmpty() && itemId != null) {
						item = itemRepository.findById(itemId).orElse(null);
						item.addSnippet(snippet);
						itemRepository.save(item);
					}
					
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
