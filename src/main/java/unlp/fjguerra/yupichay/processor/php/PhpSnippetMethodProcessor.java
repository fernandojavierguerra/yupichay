package unlp.fjguerra.yupichay.processor.php;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.domain.Snippet;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.SnippetRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;
import unlp.fjguerra.yupichay.util.FileUtility;

public class PhpSnippetMethodProcessor extends PhpProcessor {

	public PhpSnippetMethodProcessor() {
	}

	@Override
	public void findAnnotations(Project project, SymbolRepository symbolRepository,
			SnippetRepository snippetRepository, ItemRepository itemRepository) {
		//TODO
		List<String> fileList = this.getProjectFilesPath(project.getInputPath());
		FileUtility fileUtility = new FileUtility();
		String regexAnnotation = "@unlp.fjguerra.yupichay.annotations.Trace.*notion.*symboltype.*";
		String regexClass = "class*\\s+([a-zA-Z0-9_]+)";
		String regexMethods = "function*\\s+([a-zA-Z0-9_]+)";
		for (String file : fileList) {
			try {
				Integer classLineNumber = fileUtility.getLineNumber(file, regexClass);

//				To search after class declaration
				Integer annotationLineNumber = fileUtility.getLineNumber(file, regexAnnotation, classLineNumber);
				
				while (annotationLineNumber > 0 && classLineNumber > 0) {
//					Search for method after annotation					
					Integer methodLineNumber = fileUtility.getLineNumber(file, regexMethods, annotationLineNumber);
					String methodDeclaration = fileUtility.getLine(file, methodLineNumber);
					String methodName = this.getElementName(methodDeclaration, regexMethods);
					
					String qualifiedName = methodName;
					String source = fileUtility.getLines(file, annotationLineNumber);
					
					Snippet snippet = snippetRepository.findByQualifiedName(qualifiedName).orElse(
							new Snippet(qualifiedName, methodName, annotationLineNumber.toString(), source, project, file));
					
					String annotationCodeLine = fileUtility.getLine(file, annotationLineNumber);
//					System.out.println("Annotation: " + annotationCodeLine);
					String notion = this.getNotion(annotationCodeLine);
					String symbolType = this.getSymbolType(annotationCodeLine);
					
					Symbol symbol = symbolRepository.findByNotionAndSymbolType(notion, symbolType);
					// Asociar snippet y anotacion
					snippetRepository.save(snippet);
					symbol.addSnippet(snippet);
					symbolRepository.save(symbol);
					//TODO asociar item
					
//					Search after last method
					annotationLineNumber = fileUtility.getLineNumber(file, regexAnnotation, methodLineNumber);
					
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}	

}
