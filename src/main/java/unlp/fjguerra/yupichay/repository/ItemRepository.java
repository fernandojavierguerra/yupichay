package unlp.fjguerra.yupichay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import unlp.fjguerra.yupichay.domain.Item;

public interface ItemRepository extends JpaRepository<Item, String> {

	List<Item> findByParentItemIsNull();

	@Query(value = "SELECT * FROM item WHERE fk_parent_item IS NULL AND id IN (SELECT DISTINCT fk_parent_item FROM item WHERE fk_parent_item IS NOT NULL)", nativeQuery = true)
	List<Item> findEpicItems();

	List<Item> findByTextContainingIgnoreCase(String search);



	

}
