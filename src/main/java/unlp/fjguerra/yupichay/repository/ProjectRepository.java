package unlp.fjguerra.yupichay.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import unlp.fjguerra.yupichay.domain.Project;

public interface ProjectRepository extends JpaRepository<Project, Long> {

}
