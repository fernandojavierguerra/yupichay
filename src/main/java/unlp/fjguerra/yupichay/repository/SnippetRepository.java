package unlp.fjguerra.yupichay.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Snippet;

public interface SnippetRepository extends JpaRepository<Snippet, Long> {

//	Optional <Snippet> findbyClassname(String classname);

	Optional <Snippet> findByQualifiedName(String qualifiedName);

}
