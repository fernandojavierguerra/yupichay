package unlp.fjguerra.yupichay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import unlp.fjguerra.yupichay.domain.Symbol;

public interface SymbolRepository extends JpaRepository<Symbol, Long> {

	Symbol findByNotion(String value);

	Symbol findFirstByNotion(String value);

	Symbol findByNotionAndSymbolType(String value, String symbolType);

	List<Symbol> findBySymbolType(String string);

	List<Symbol> findByOrderByNotion();

}
