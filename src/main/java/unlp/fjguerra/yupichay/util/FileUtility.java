package unlp.fjguerra.yupichay.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class FileUtility {
	public void insertBefore(String target, String insertString, String fileName, String typeTarget,
			Integer lineNumber) {
		try {
			// input the file content to the StringBuffer "input"
			BufferedReader file = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuffer inputBuffer = new StringBuffer();
			Integer lineN = 0;

			while ((line = file.readLine()) != null) {
				lineN++;
//				if ((line.contains(typeTarget)) && (line.contains(target))) {
				if (lineN == lineNumber) {
					inputBuffer.append(insertString);
					inputBuffer.append('\n');
				}

				inputBuffer.append(line);
				inputBuffer.append('\n');
			}
			String inputStr = inputBuffer.toString();

			file.close();

			// check if the new input is right
			System.out.println("----------------------------------\n" + inputStr);

			// write the new String with the replaced line OVER the same file
			FileOutputStream fileOut = new FileOutputStream(fileName);
			fileOut.write(inputStr.getBytes());
			fileOut.close();

		} catch (Exception e) {
			System.out.println("Problem reading file.");
		}
	}

	public void searchInside(String target, String insertString, String fileName, String typeTarget,
			Integer lineNumber) {
		try {
			// input the file content to the StringBuffer "input"
			BufferedReader file = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuffer inputBuffer = new StringBuffer();
			Integer lineN = 0;

			while ((line = file.readLine()) != null) {
				lineN++;
//				if ((line.contains(typeTarget)) && (line.contains(target))) {
				if (lineN == lineNumber) {
					inputBuffer.append(insertString);
					inputBuffer.append('\n');
				}

				inputBuffer.append(line);
				inputBuffer.append('\n');
			}
			String inputStr = inputBuffer.toString();

			file.close();

			// check if the new input is right
			System.out.println("----------------------------------\n" + inputStr);

			// write the new String with the replaced line OVER the same file
			FileOutputStream fileOut = new FileOutputStream(fileName);
			fileOut.write(inputStr.getBytes());
			fileOut.close();

		} catch (Exception e) {
			System.out.println("Problem reading file.");
		}
	}

	public String getSourceCode(String source, Integer start, Integer end) {
		String[] lines = source.split(System.getProperty("line.separator"));
		String output = "";
		for (Integer i = start; i < end; i++) {
//			System.out.println("line " + lines[i]);
			output = output + lines[i] + System.getProperty("line.separator");
		}
		return output;
	}

	public String getLines(String fileName, Integer lineNumber) {
		try {
			// input the file content to the StringBuffer "input"
			BufferedReader file = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuffer inputBuffer = new StringBuffer();
			Integer lineN = 0;

			while ((line = file.readLine()) != null) {
				lineN++;

				if (lineN >= lineNumber - 5 && lineN <= lineNumber + 5) {
					inputBuffer.append(line);
					inputBuffer.append('\n');
//					inputBuffer.append(System.getProperty("line.separator"));
				}

			}
			String inputStr = inputBuffer.toString();
			System.out.println("----------------------------------\n" + inputStr);
			file.close();
			return inputStr;

		} catch (Exception e) {
			System.out.println("Problem reading file.");
		}
		return null;
	}

	public String getLine(String fileName, Integer lineNumber) {
		Scanner s;
		try {
			s = new Scanner(new File(fileName));
			for (int i = 0; i < lineNumber - 1; i++) // Discard n-1 lines
				s.nextLine();
			return s.nextLine();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * Read file as String
	 * 
	 * @param file Then Name of the file
	 * @return
	 * @throws IOException
	 */
	public String readFileToString(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}

			return stringBuilder.toString();
		} finally {
			reader.close();
		}
	}

	public Integer findClosingCharPosition(String expression, Integer index, Character opening, Character closing) {
		Integer i;

		// If index given is invalid and is
		// not an opening bracket.

		if (expression.charAt(index) != opening) {
			System.out.print(expression + ", " + index + ": -1\n");
			return -1;
		}

		// Stack to store opening brackets.
		Stack<Integer> st = new Stack<>();

		// Traverse through string starting from
		// given index.
		for (i = index; i < expression.length(); i++) {

			// If current character is an
			// opening bracket push it in stack.
			if (expression.charAt(i) == opening) {
				st.push((int) expression.charAt(i));
			} // If current character is a closing
				// bracket, pop from stack. If stack
				// is empty, then this closing
				// bracket is required bracket.
			else if (expression.charAt(i) == closing) {
				st.pop();
				if (st.empty()) {
					System.out.print(expression + ", " + index + ": " + i + "\n");
					return i;
				}
			}
		}

		// If no matching closing bracket
		// is found.
		System.out.print(expression + ", " + index + ": -1\n");
		return -1;
	}

	/**
	 * 
	 * Return the first line number that matches with regular expression
	 * 
	 * @param fileName       File Path
	 * @param regex          Regular expression to search for
	 * @param fromLineNumber Start line
	 * @return
	 * @throws FileNotFoundException
	 */
	public Integer getLineNumber(String fileName, String regex, Integer fromLineNumber) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(fileName));
		Pattern pattern = Pattern.compile(regex);
		int lineNumber = 1;
//		if(regex == "(\\n\\s*)function"){
//			System.out.println("regex: " + "(\\n\\s*)function\\s.*listAll");
//		}

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (lineNumber >= fromLineNumber) {

				Matcher m = pattern.matcher(line);
				// if(m.matches()){
				if (m.find()) {
					// System.out.println("line " + lineNumber);
					sc.close();
					return lineNumber;
				}
			}
			lineNumber++;

		}
		sc.close();
		return -1;

	}

	public Integer getLineNumber(String fileName, String regex) throws FileNotFoundException {
		return this.getLineNumber(fileName, regex, 1);
	}

	public String getTextInsideDelimiters(String expresssion, String fieldName) {
		Integer beginIndex = expresssion.indexOf('(', expresssion.indexOf(fieldName));
		Integer endIndex = this.findClosingCharPosition(expresssion, beginIndex, '(', ')');
		System.out.println("Text inside " + expresssion.substring(beginIndex, endIndex));
		return expresssion.substring(beginIndex + 1, endIndex);
	}

	/**
	 * 
	 * Return All elements (name, position) from file that matches regex
	 * 
	 * @param file
	 * @param regexElement
	 * @return
	 */
	public Map<Integer, String> getAllElementsName(String fileName, String regexElement) {
		Map<Integer, String> elements = new HashMap<Integer, String>();
		Pattern pattern = Pattern.compile(regexElement);
		Integer lineNumber = 1;
		String elementName = "";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				Matcher matcher = pattern.matcher(line);
//				System.out.println(line);
				if (matcher.find()) {
//					System.out.println("line " + lineNumber);
//					System.out.println("name element " + matcher.group(1));
					elementName = matcher.group(1);
					elements.put(lineNumber, elementName);
				}
				lineNumber++;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return elements;
	}

}
