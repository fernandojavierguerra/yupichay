package unlp.fjguerra.yupichay;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lifia.lelparser.exceptions.InvalidRequirementFormatException;
import lifia.lelparser.model.NLParser;
import lifia.lelparser.model.parserResults.ParserResult;
import unlp.fjguerra.yupichay.domain.Item;
import unlp.fjguerra.yupichay.domain.Project;
import unlp.fjguerra.yupichay.domain.Symbol;
import unlp.fjguerra.yupichay.repository.ItemRepository;
import unlp.fjguerra.yupichay.repository.ProjectRepository;
import unlp.fjguerra.yupichay.repository.SymbolRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class YupichayApplicationTests {

	@Autowired
	private SymbolRepository symbolRepository;
	@Autowired
	private ItemRepository itemRepository;
	@Autowired
	private ProjectRepository projectRepository;
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void testProject() throws InvalidRequirementFormatException {
		System.out.println("Begin Test");
		NLParser parser = new NLParser();
		Project project = new Project();

		ParserResult result = parser.parseBatchUserStories(project.buildSampleUserStories());
		System.out.println("End Test");
	}

	@Test
	public void testPersistence() {
		
		System.out.println("Poblar la BD");
    	
    	Project proyecto = new Project();
    	projectRepository.save(proyecto);

    	
		Item item1 = new Item("US#3",
				"As an estimator I want to join a game by entering my name on the page I received the url for So that I can participate", proyecto);

		Symbol symbol1 = new Symbol("estimator", "subject", proyecto);
		
		symbolRepository.save(symbol1);
		itemRepository.save(item1);
		item1.addSymbol(symbol1);
		//symbolRepository.save(symbol1);
		itemRepository.save(item1);
		Symbol symbolRecuperado = symbolRepository.findById((long) 1).orElse(null);
		//System.out.println("Simbolo:" + symbolRecuperado.toString());

	}

}
