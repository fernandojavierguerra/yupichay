package unlp.fjguerra.yupichay.phpparser;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.thm.mni.ii.phpparser.PHPParser;
import de.thm.mni.ii.phpparser.ast.Basic;
import unlp.fjguerra.yupichay.util.FileUtility;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Phpparser {
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void phpCode() {
		PHPParser.Result res = (PHPParser.Result) PHPParser.parse("<?php $value = 5;");
        if (res instanceof PHPParser.Success) {
            Basic.Script s = ((PHPParser.Success) res).script();
            System.out.println(s);
        } else if (res instanceof PHPParser.Failure) {
            String msg = ((PHPParser.Failure) res).fullMsg();
            System.out.println(msg);
        }
		
	}
	
	@Test
	public void phpFile() {
		FileUtility file = new FileUtility();
		try {
			String phpfile = file.readFileToString("./src/main/resources/project/phpexample/Name.php");
			System.out.println(phpfile);
			PHPParser.Result res = (PHPParser.Result) PHPParser.parse(phpfile);
	        if (res instanceof PHPParser.Success) {
	            Basic.Script s = ((PHPParser.Success) res).script();
	            System.out.println(s);
	        } else if (res instanceof PHPParser.Failure) {
	            String msg = ((PHPParser.Failure) res).fullMsg();
	            System.out.println(msg);
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void parsePhpFile() {
		FileUtility file = new FileUtility();
		try {
			String phpfile = file.readFileToString("./src/main/resources/project/Faker/src/Faker/Provider/en_US/Address.php");
			System.out.println(phpfile);
			PHPParser.Result res = (PHPParser.Result) PHPParser.parse(phpfile);
            Basic.Script s = ((PHPParser.Success) res).script();
            System.out.println(s);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
