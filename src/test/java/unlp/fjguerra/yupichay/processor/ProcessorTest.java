package unlp.fjguerra.yupichay.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.lang.annotation.Annotation;
import java.util.List;

import org.junit.Test;

import spoon.Launcher;
import spoon.reflect.code.CtLiteral;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.factory.Factory;

import unlp.fjguerra.yupichay.testclasses.repeatable.Repeated;

public class ProcessorTest {
	
//	@Test
//	public void testAnnotationInClassManaged() {
//
//		Launcher spoon = new Launcher();
//		spoon.addInputResource("./src/test/java/unlp/fjguerra/yupichay/testclasses/repeatable");
//	
//		ClassProcessor classProcessor = new ClassProcessor();
//		spoon.addProcessor(classProcessor);
//
//		spoon.run();
//
//		for (CtType type : classProcessor.elements) {
//			System.out.println("Clase: " + type.getSimpleName());
//			System.out.println("quali: " + type.getQualifiedName());
//			System.out.println("position: " + type.getPosition());
//
//			List<CtAnnotation<?>> annotations = type.getAnnotations();
//			for (CtAnnotation a : annotations) {
//				if (a.getAnnotationType().getSimpleName().equals("Trace")) {
//					System.out.println(a.getValue("notion"));
//					System.out.println(a.getValue("symboltype"));
//				}
//			
//			}
//			
//		}
//
//	}
//	
//	@Test
//	public void testProcessorWithGenericType() {
//
//		Launcher spoon = new Launcher();
//
//		spoon.addInputResource("./src/test/java/unlp/fjguerra/yupichay/imports/testclasses");
//		CtClassProcessor classProcessor = new CtClassProcessor();
//		spoon.addProcessor(classProcessor);
//
//		spoon.run();
//
//		assertFalse(classProcessor.elements.isEmpty());
//
//		for (CtType type : classProcessor.elements) {
//			assertTrue("Type " + type.getSimpleName() + " is not a class", type instanceof CtClass);
//		}
//	}
//
//	@Test
//	public void testRepeatableAnnotationAreManaged() {
//		// contract: when two identical repeatable annotation are used, they should be
//		// displayed in two different annotations and not factorized
//		Launcher spoon = new Launcher();
//		spoon.addInputResource("./src/test/java/unlp/fjguerra/yupichay/testclasses/repeatable");
//		spoon.buildModel();
//
//		CtType type = spoon.getFactory().Type().get(Repeated.class);
//		
////		CtMethod firstMethod = (CtMethod) type.getMethodsByName("method").get(0);
////		CtClass firstMethod = (CtClass) type.getClass().
//		List<CtAnnotation<?>> annotations = type.getAnnotations();
////		List<CtAnnotation<?>> annotations = firstMethod.getAnnotations();		
//
//		assertEquals(4, annotations.size());
//
////		for (CtAnnotation a : annotations) {
////			assertEquals("Trace", a.getAnnotationType().getSimpleName());
////		}
////
////		String classContent = type.toString();
////		assertTrue("Content of the file: " + classContent,
////				classContent.contains("@unlp.fjguerra.yupichay.testclasses.repeatable.Tag(\"machin\")"));
////		assertTrue("Content of the file: " + classContent,
////				classContent.contains("@unlp.fjguerra.yupichay.testclasses.repeatable.Tag(\"truc\")"));
//
//	}
//	
//	@Test
//	public void testRepeatSameAnnotationOnClass() {
//		final Launcher launcher = new Launcher();
//		launcher.addInputResource("./src/test/java/unlp/fjguerra/yupichay/testclasses/AnnotationsRepeated.java");
//		launcher.buildModel();
//		Factory factory = launcher.getFactory();
//		final CtClass<?> ctClass = (CtClass<?>) factory.Type().get(AnnotationsRepeated.class);
//
//		final List<CtAnnotation<? extends Annotation>> annotations = ctClass.getAnnotations();
//		assertEquals("Class must to have multi annotation of the same type", 1, annotations.size());
//		assertSame("Type of the first annotation is AnnotationRepeated", AnnotationRepeated.class,
//				annotations.get(0).getAnnotationType().getActualClass());
////		assertSame("Type of the second annotation is AnnotationRepeated", AnnotationRepeated.class, annotations.get(1).getAnnotationType().getActualClass());
//		assertEquals("Argument of the first annotation is \"First\"", "First",
//				((CtLiteral) annotations.get(0).getValue("value")).getValue());
////		assertEquals("Argument of the second annotation is \"Second\"", "Second", ((CtLiteral) annotations.get(1).getValue("value")).getValue());
//	}

}
