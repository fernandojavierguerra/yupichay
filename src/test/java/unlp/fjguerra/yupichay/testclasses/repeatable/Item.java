package unlp.fjguerra.yupichay.testclasses.repeatable;

import unlp.fjguerra.yupichay.annotations.Trace;

@Trace(notion = "estimator", symboltype = "subject", item = "")
@Trace(notion = "game", symboltype = "object", item = "")


public class Item {

    public void joinMethod(String s) {}

    public void itemWithoutAnnotation() {

    }
}
