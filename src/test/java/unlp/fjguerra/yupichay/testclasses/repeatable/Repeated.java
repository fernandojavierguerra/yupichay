package unlp.fjguerra.yupichay.testclasses.repeatable;

import unlp.fjguerra.yupichay.annotations.Trace;
import unlp.fjguerra.yupichay.annotations.Traces;

@Tag("machin")
@Tag("truc")
@Trace(notion = "estimator", symboltype = "subject",  item = "")
@Trace(notion = "game", symboltype = "object",  item = "")
public class Repeated {

    public void method() {}

    public void withoutAnnotation() {

    }
}
