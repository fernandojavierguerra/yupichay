package unlp.fjguerra.yupichay.testclasses.repeatable;

public @interface Tags {
    Tag[] value();
}
