package src;

/**
 * Documented class
 */
public class DocTestII {
    /**
     * Document method
     */
    public void toto() {}

    public void totoUndocumented() {}

    /**
     * Documented method
     */
    protected void totoProtected() {}
    /**
     * Documented method
     */
    protected void totoProtectedUndocumented() {}

    /**
     * Documented field
     */
    protected boolean field;

    public String fieldNotDocumented;
}